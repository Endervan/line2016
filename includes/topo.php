

<div class="container">
	<div class="row topo_geral">
		<!--  ==============================================================  -->
		<!-- CONTATOS -->
		<!--  ==============================================================  -->
		<div class="col-xs-8 text-right top10 contatos">
			<div class="pull-right left10">
			<div class="pull-right left10 top5">
				<h4 style="font-size:20px;">
					<span><?php Util::imprime($config[ddd1]); ?></span> <?php Util::imprime($config[telefone1]); ?>

					<?php if (!empty($config[telefone3])): ?>
							<span> <i style="font-size:20px;" class="fa fa-whatsapp" aria-hidden="true"></i> <?php Util::imprime($config[ddd3]); ?></span> <?php Util::imprime($config[telefone3]); ?>
					<?php endif; ?>

				</h4>
			</div>
			<div class="pull-right top10">
				<h3><i class="fa fa-phone-square right10"></i>ATENDIMENTO:</h3>
			</div>
		</div>
		</div>
		<!--  ==============================================================  -->
		<!-- CONTATOS -->
		<!--  ==============================================================  -->

<?php /*
		<!--  ==============================================================  -->
		<!-- BARRA PESQUISA -->
		<!--  ==============================================================  -->
		<div class="col-xs-4 pesquisa">
			<form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
				<div class="input-group stylish-input-group">
					<input type="text" class="form-control" name="busca_produtos" placeholder="BUSCAR" >
					<span class="input-group-addon">
						<button type="submit">
							<span class="glyphicon glyphicon-search"></span>
						</button>
					</span>
				</div>
			</form>
		</div>

	</div>
</div>
<!--  ==============================================================  -->
<!-- BARRA PESQUISA -->
<!--  ==============================================================  -->
*/ ?>

<!--  ==============================================================  -->
<!--CARRINHO-->
<!--  ==============================================================  -->
<div class="col-xs-4 div_personalizado dropdown text-right">
	<button class="btn btn_carrinho dropdown-toggle col-xs-12" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		<span class="pull-left">
       <i class="fa fa-cart-arrow-down"></i>
		</span>
		<span class="pt20">	Meu Orçamento</span>
		<span class="pull-right top5">
			<i class='fa fa-angle-double-down'></i>
		</span>
		<span class="pull-left">
			<a  class="item_selicionavel"><span class="badge"><?php echo count($_SESSION[solicitacoes_produtos]); ?></span></a>
		</span>
	</button>


	<div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dropdownMenu1">


		<?php
		if(count($_SESSION[solicitacoes_produtos]) > 0)
		{
			for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
			{
				$row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
				?>
				<div class="lista-itens-carrinho">
					<div class="col-xs-2">
						<?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"$row[titulo]")) ?>
					</div>
					<div class="col-xs-8">
						<h1><?php Util::imprime($row[titulo]) ?></h1>
					</div>
					<div class="col-xs-1">
						<a href="<?php echo Util::caminho_projeto() ?>/orcamentos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
					</div>
				</div>
				<?php
			}
		}
		?>







		<div class="text-right bottom20">
			<a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="ENVIAR ORÇAMENTO" class="btn btn-vermelho" >
				ENVIAR ORÇAMENTO
			</a>
		</div>

	</div>
</div>
<!--  ==============================================================  -->
<!--CARRINHO-->
<!--  ==============================================================  -->

</div>
</div>


<div class="container bottom5">

	<div class="row">
		<!--  ==============================================================  -->
		<!-- LOGO -->
		<!--  ==============================================================  -->
		<div class="col-xs-3 posicao-logo">
			<a href="<?php echo Util::caminho_projeto() ?>/" title="">
				<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.png" alt="">
			</a>
		</div>
		<!--  ==============================================================  -->
		<!-- LOGO -->
		<!--  ==============================================================  -->

		<!--  ==============================================================  -->
		<!-- MENU-->
		<!--  ==============================================================  -->
		<div class="col-xs-9  text-right">
			<div class="menu_topo">
				<ul class="top15 ">
				<li class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>">
				<a href="<?php echo Util::caminho_projeto() ?>/">
					<img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-home.jpg" alt="">
				</a></li>

				<li class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>">
				<a href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA</a></li>

				<li class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>">
				<a href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a></li>

				<li class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>">
				<a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a></li>

				<li class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>">
				<a href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a></li>

				<li class="<?php if(Url::getURL( 0 ) == "contatos"){ echo "active"; } ?>">
				<a href="<?php echo Util::caminho_projeto() ?>/contatos">CONTATOS</a></li>

				<li>
				<a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a></li>
			</ul>
			</div>
		</div>
		<!--  ==============================================================  -->
		<!-- MENU-->
		<!--  ==============================================================  -->

	</div>
</div>

<!--  ==============================================================  -->
<!--SEGUNDO MENU TOPO-->
<!--  ==============================================================  -->
<div class="container-fluid fundo_cinza_escuro">
	<div class="row">
		<div class="container">
			<div class="row">
				<!--  ==============================================================  -->
				<!-- MENU-->
				<!--  ==============================================================  -->
				<div class="col-xs-12 lista-categorias text-center">
					<!-- Note that the .navbar-collapse and .collapse classes have been removed from the #navbar -->
					<div id="navbar">
						<ul class="nav navbar-nav">

							<?php
							$result = $obj_site->select("tb_categorias_produtos", "order by rand()");
							if (mysql_num_rows($result) > 0) {
								while($row = mysql_fetch_array($result)){
									?>
									<li>
										<a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
											<span class="clearfix"><i class="fa fa-circle right10"></i><?php Util::imprime($row[titulo]); ?></span>
										</a>
									</li>
									<?php
								}
							}
							?>







						</ul>
					</div><!--/.nav-collapse -->
				</div>
				<!--  ==============================================================  -->
				<!-- MENU-->
				<!--  ==============================================================  -->



			</div>
		</div>
	</div>
</div>
<!--  ==============================================================  -->
<!--SEGUNDO MENU TOPO-->
<!--  ==============================================================  -->
