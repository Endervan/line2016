<div class="clearfix"></div>
<div class="container-fluid rodape top50">
	<div class="row">

		<!-- menu -->
		<div class="container top30">
			<div class="row">
				<div class="col-xs-12 bg-menu-rodape">
					<ul class="menu-rodape">
						<li><a class="<?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "servicos" or Url::getURL( 0 ) == "servico"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/servicos">SERVIÇOS</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "contatos"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/contatos">CONTATOS</a></li>
						<li><a class="<?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>" href="<?php echo Util::caminho_projeto() ?>/trabalhe-conosco">TRABALHE CONOSCO</a></li>
					</ul>
				</div>

				<!-- menu -->

				<!--  endereco, telefone -->
				<div class="col-xs-8 top30">
					<div>
						<p><i class="glyphicon glyphicon-home"></i><?php Util::imprime($config[endereco]); ?></p>
					</div>

				</div>
				<div class="col-xs-4 top20">
					<p>
						<i class="glyphicon glyphicon-earphone"></i>
						<?php Util::imprime($config[telefone1]); ?>

						<?php if (!empty($config[telefone2])) { ?>
							 / <?php Util::imprime($config[telefone2]); ?>
						<?php } ?>

						<?php if (!empty($config[telefone3])) { ?>
							 / <?php Util::imprime($config[telefone3]); ?>
						<?php } ?>

						<?php if (!empty($config[telefone4])) { ?>
							 / <?php Util::imprime($config[telefone4]); ?>
						<?php } ?>
					</p>
				</div>
				<!-- endereco, telefone -->

				<div class="clearfix"></div>

				<!-- logo -->
				<div class="col-xs-8 top15">
					<div class="col-xs-3 top10">
					<a href="<?php echo Util::caminho_projeto() ?>">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/grupo_rodape01.png" alt="">
					</a>
				</div>

				<div class="col-xs-3 top10">
					<a href="<?php echo Util::caminho_projeto() ?>">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/grupo_rodape02.png" alt="">
					</a>
				</div>

				<div class="col-xs-3 top10">
					<a href="<?php echo Util::caminho_projeto() ?>">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/grupo_rodape03.png" alt="">
					</a>
				</div>

				<div class="col-xs-3 top10">
					<a href="<?php echo Util::caminho_projeto() ?>">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/grupo_rodape04.png" alt="">
					</a>
				</div>
				</div>
				<!-- logo -->


				<div class="col-xs-4 text-right">
					<?php if ($config[google_plus] != "") { ?>
						<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
							<i class="fa fa-google-plus right15" style="color: #000"></i>
						</a>
					<?php } ?>

					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-masmidia.png"  alt="">
					</a>
				</div>

			</div>
		</div>
	</div>
</div>


<div class="container-fluid rodape-preto">
	<div class="row">
		<div class="col-xs-12 text-center">
			<h5>© Copyright GRUPO LINE</h5>
		</div>
	</div>
</div>
