
<?php
require_once("../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>
<!doctype html>
<html>

<head>
  <?php require_once('./includes/head.php'); ?>

  <script type="text/javascript">
    $(document).ready(function() {
      $('#Carousel').carousel({
        interval: 5000
      })
    });
  </script>

  

</head>

<body>

  <?php require_once('./includes/topo.php'); ?>


  <!-- ======================================================================= -->
  <!-- BG HOME    -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row carroucel-home">

      <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

        <!--  ==============================================================  -->
        <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
        <!--  ==============================================================  -->
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <!-- Indicators -->
              <ol class="carousel-indicators">
                <?php
                $result = $obj_site->select("tb_banners", "and tipo_banner = 2 ");
                if(mysql_num_rows($result) > 0){
                  $i = 0;
                  while ($row = mysql_fetch_array($result)) {
                    $imagens[] = $row;
                    ?> 
                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){ echo "active"; } ?>"></li>
                    <?php 
                    $i++;
                  }
                }
                ?>
              </ol>

            

            </div>
          </div>
        </div> 
        <!--  ==============================================================  -->
        <!-- ARRUSTE BOLINHAS E SETA DENTRO GRID PARA QLQ RESOLUCAO-->
        <!--  ==============================================================  --> 
        

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">


          <?php 
          if (count($imagens) > 0) {
            $i = 0;
            foreach ($imagens as $key => $imagem) {
              ?>
              <div class="item <?php if($i == 0){ echo "active"; } ?>">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]); ?>" alt="<?php Util::imprime($imagem[titulo]); ?>">
                <div class="carousel-caption">

                  <div class="container">
                    <div class="row">
                      <div class="col-xs-12 text-right">
                        <h1><?php Util::imprime($imagem[titulo]); ?></h1>
                        <div class="top15">
                          <p><?php Util::imprime($imagem[legenda]); ?></p>

                          <?php if (!empty($imagem[url])): ?>
                          <div class="top5">
                            <a class="btn btn_saiba_home" href="<?php Util::imprime($imagem[url]); ?>" role="button">
                             <span class="pull-right"> SAIBA MAIS</span>
                           </a>
                         </div>
                       <?php endif; ?>

                     </div>
                   </div>
                 </div>
               </div>

             </div>
           </div>
           <?php
           $i++;
         }
       }
       ?>

     </div>


   </div>

   <div class="container_banner_faixa"></div>

 </div>
</div>
<!-- ======================================================================= -->
<!-- BG HOME    -->
<!-- ======================================================================= -->
<div class="clearfix"></div>

<!--  ==============================================================  -->
<!--barra categorias home-->
<!--  ==============================================================  -->
<div class="container">
 <div class="row barra_categorias">
   <div class="col-xs-12">
    <select name="select_categorias" class="selectpicker" data-max-options="1" data-live-search="true" title="TODAS AS NOSSAS CATEGORIAS" data-width="100%">
      <optgroup label="SELECIONE">
        <?php 
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while($row = mysql_fetch_array($result)){
            ?>
              <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" data-tokens="<?php Util::imprime($row[titulo]); ?>"><?php Util::imprime($row[titulo]); ?></option>
            <?php 
            }
          }
          ?>
      </optgroup>
    </select>
  </div>
</div>
</div>
<!--  ==============================================================  -->
<!--barra categorias home-->
<!--  ==============================================================  -->





<!--  ==============================================================  -->
<!--PRODUTOS HOME-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row produtos_home">


   <!--  ==============================================================  -->
   <!-- ITEM 01 -->
   <!--  ==============================================================  -->
   <?php 
   $result = $obj_site->select("tb_produtos", "order by ordem limit 4");
   if (mysql_num_rows($result) > 0) {
    $i = 0;
    while($row = mysql_fetch_array($result)){
      ?>
      <div class="col-xs-6 top10" >
        <div class="thumbnail">
          <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 205, 128, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
          </a>          <div class="caption">
          <h1><?php Util::imprime($row[titulo]); ?></h1>
          <div class="top10 bottom10">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn_produtos_home_geral pt5" title="Saiba mais">Saiba Mais</a> 

            <a class="btn btn-default btn_produtos_home_geral" role="button" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_carrinho_home.png" alt="">
            </a>
          </div>
        </div>
      </div>
    </div>
    <?php
    if ($i == 1) {
      echo '<div class="clearfix"></div>';
      $i = 0;
    }else{
      $i++;
    }
  }
}
?>
</div>
</div>
<!--  ==============================================================  -->
<!-- PRODUTOS HOME -->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!--  OUTROS SERVICOS HOME-->
<!--  ==============================================================  -->
<div class="container bottom40">
  <div class="row">
    <div class="col-xs-12 servicos_home top30 bottom30">
      <h5 class="icon-faixa pull-left">OS NOSSOS SERVIÇOS</h5>

      <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos" title="Ver todos" class="pull-right">
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn-ver-todos.jpg" alt="Ver todos">
      </a>
    </div>

    <div class="servicos_home_cat ">
      <!--  ==============================================================  -->
      <!--  ITEM 01-->
      <!--  ==============================================================  -->
      <?php 
      $result = $obj_site->select("tb_servicos", "order by rand() limit 5");
      if (mysql_num_rows($result) > 0) {
        $i = 0;
        while($row = mysql_fetch_array($result)){
          ?> 
          <div class="col-xs-12 bottom40">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <div class="media">
                <div class="media-left media-middle">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",50, 48, array("class"=>"media-object", "alt"=>"$row[imagem]")) ?>
                </div>
                <div class="media-body ">
                  <h1 class="media-heading"><?php Util::imprime($row[titulo]); ?>
                  </h1>
                </div>
              </div>
            </a>
          </div>
          <?php
          if ($i == 1) {
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
        }
      }
      ?>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!--  OUTROS SERVICOS HOME-->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!--SAIBA MAIS MATERIAIS-->
<!--  ==============================================================  -->
<div class="container ">
  <div class="row saiba_mais_materiais">
    <div class="col-xs-12 top40 text-center">
      <h5 class="pg60">SOBRE A LINE MATERIAIS ELÉTRICOS</h5>


      <div class="saiba_mais_desc top35">
        <div class="saiba_mais_desc">
          <div class=" pg10">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 9) ?>
            <p><?php Util::imprime($row[descricao], 500000); ?></p>
          </div>


          <div class="top10 text-left">
            <h6 class="bottom20">COMO CHEGAR</h6>
            <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="290" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>

          <div class="pull-left top10">
            <h3><i class="fa fa-phone-square right10"></i>ATENDIMENTO:</h3>
          </div>

          <div class="pull-left left10">
            <h4>
              <span>
                <?php Util::imprime($config[telefone1]); ?>
              </span>
            </h4>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!--SAIBA MAIS MATERIAIS-->
<!--  ==============================================================  -->



<div class="clearfix"></div>


<!--  ==============================================================  -->
<!--NOSSA DICAS-->
<!--  ==============================================================  -->
<div class="container top30">
  <div class="row dicas_home">
    
    <h5 class="icon-faixa pull-left">AS NOSSAS DICAS</h5>

    <!--  ==============================================================  -->
    <!--  ITEM 01-->
    <!--  ==============================================================  -->
     <?php 
      $result = $obj_site->select("tb_dicas", "order by rand() limit 3");
      if (mysql_num_rows($result) > 0) {
        $i = 0;
        while($row = mysql_fetch_array($result)){
        ?>  
        <div class="col-xs-12 top30">
              <a href="<?php echo Util::caminho_projeto() ?>mobile//dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <div class="media">
              <div class="media-left media-middle">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 102, 102, array("class"=>"media-object img-circle", "alt"=>"$row[imagem]")) ?>
             </div>
             <div class="media-body">
              <div class="top10 media-heading">
                      <p><?php Util::imprime($row[titulo]); ?></p>
              </div>
            </div>
          </div>
        </a>
      </div>
      <?php
      if ($i == 1) {
        echo '<div class="clearfix"></div>';
        $i = 0;
      }else{
        $i++;
      }
    }
  }
  ?>

</div>
</div>
<!--  ==============================================================  -->
<!--NOSSA DICAS-->
<!--  ==============================================================  -->



<?php require_once('./includes/rodape.php'); ?>

</body>

</html>


