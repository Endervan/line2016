
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",16) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 164px no-repeat;
  }
</style>





<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- TITULO BG INTERNAS  -->
  <!-- ======================================================================= -->

  <div class="container alt_pag_internas">
    <div class="row">
      <div class="container_banner_faixa_internas"></div>
      <div class="col-xs-12 text-center top150">
       <div class="col-xs-offset-2  titulo_pagina_internas effect2">
         <h1 class="pt15"> NOSSOS SERVIÇOS</h1>
         
       </div>
     </div>
   </div>
 </div>
 <!-- ======================================================================= -->
 <!-- TITULO BG INTERNAS  -->
 <!-- ======================================================================= -->

<!-- ======================================================================= -->
  <!-- descricao servico  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row bg-cinza">
      <div class="col-xs-12">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 7) ?>
            <p><?php Util::imprime($row[descricao],400); ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- descricao servico  -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- LISTA SERVICO  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">
      
          <?php 
          $result = $obj_site->select("tb_servicos");
          if (mysql_num_rows($result) > 0) {
            while($row = mysql_fetch_array($result)){
            ?>
              <div class="col-xs-12 top40">
                <div class="lista-servico">
                  <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    
                    <div class="col-xs-2">
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 68, 60, array("class"=>"", "alt"=>"$row[imagem]")) ?>
                    </div>

                    <div class="col-xs-10">
                        <h4><?php Util::imprime($row[titulo]); ?></h4>
                        <p><?php Util::imprime($row[descricao], 200); ?></p>
                    </div>

                  </a>
                </div>
                
              </div>
            <?php 
            }
          }
          ?>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- LISTA SERVICO  -->
  <!-- ======================================================================= -->







<?php require_once('../includes/rodape.php'); ?>

</body>

</html>