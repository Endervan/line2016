
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",12) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 164px no-repeat;
  }
</style>





<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- TITULO BG INTERNAS  -->
  <!-- ======================================================================= -->

  <div class="container alt_pag_internas">
    <div class="row">
      <div class="container_banner_faixa_internas_contatos"></div>
      <div class="col-xs-12 text-center top170">
       <div class="col-xs-offset-2  titulo_pagina_internas effect2">
         <h1 class="pt15">FALE COM A <b>LINE</b></h1>
       </div>
     </div>
   </div>
 </div>
 <!-- ======================================================================= -->
 <!-- TITULO BG INTERNAS  -->
 <!-- ======================================================================= -->


 <div class="container top20">
   <div class="row">
     <!--  ==============================================================  -->
     <!--CONTATOS -->
     <!--  ==============================================================  -->
     <div class="col-xs-12 contatos">
      <h2 class="titulo-linha-baixo">ATENDIMENTO</h2>

      <div class="col-xs-6 padding0 top20">

        <div class="media">
          <div class="media-left media-middle">
            <h4><?php Util::imprime($config[telefone1]); ?></h4>
          </div>
          <div class="media-body">
            <a class="btn btn_vermelho_topo" href="tel:+55<?php Util::imprime($config[telefone1]); ?>">CHAMAR</a>
          </div>
        </div>

        </div>

        <div class="col-xs-6 padding0  top20">
          <?php if (!empty($config[telefone2])): ?>
          <div class="media">
            <div class="media-left media-middle">
              <h4><?php Util::imprime($config[telefone2]); ?></h4>
            </div>
            <div class="media-body">
              <a class="btn btn_vermelho_topo" href="tel:+55<?php Util::imprime($config[telefone2]); ?>">CHAMAR</a>
            </div>
          </div>
        <?php endif ?>
        </div>
      

    </div>
    <!--  ==============================================================  -->
    <!--CONTATOS -->
    <!--  ==============================================================  -->


    <!--  ==============================================================  -->
    <!-- FORMULARIO-->
    <!--  ==============================================================  -->
     <div class="col-xs-12 padding0">
   
          <?php
          //  VERIFICO SE E PARA ENVIAR O EMAIL
          if(isset($_POST[nome]))
          {
              $texto_mensagem = "
                                Nome: ".($_POST[nome])." <br />
                                Assunto: ".($_POST[assunto])." <br />
                                Telefone: ".($_POST[telefone])." <br />
                                Email: ".($_POST[email])." <br />
                                Mensagem: <br />
                                ".(nl2br($_POST[mensagem]))."
                                ";
              Util::envia_email($config[email], utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
              Util::envia_email($config[email_copia], utf8_decode($_POST[assunto]), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
              
              Util::alert_bootstrap("Obrigado por entrar em contato.");
              unset($_POST);
          }
          ?>



          <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data"> 
            <div class="fundo-formulario">
              <!-- formulario orcamento -->
              
                <div class="col-xs-12 top15">
                  <div class="form-group input100 has-feedback">
                    <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="NOME">
                    <span class="fa fa-user form-control-feedback top15"></span>
                  </div>
                </div>

                <div class="col-xs-12 top15">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="E-MAIL">
                    <span class="fa fa-envelope form-control-feedback top15"></span>
                  </div>
                </div>

                <div class="col-xs-12 top15">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="TELEFONE">
                    <span class="fa fa-phone form-control-feedback top15"></span>
                  </div>
                </div>

                <div class="col-xs-12 top15">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="assunto" class="form-control fundo-form1 input100" placeholder="ASSUNTO">
                    <span class="fa fa-star form-control-feedback top15"></span>
                  </div>
                </div>
        
  
                <div class="col-xs-12 top15">
                 <div class="form-group input100 has-feedback">
                  <textarea name="mensagem" cols="30" rows="8" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                  <span class="fa fa-pencil form-control-feedback top15"></span> 
                </div>
              </div>
           
            <!-- formulario orcamento -->
            <div class="col-xs-12 top15 text-right">
              <div class="top15 bottom25">
                <button type="submit" class="btn btn-formulario" name="btn_contato">
                  ENVIAR
                </button>
              </div>
            </div>

          </div>

          <!--  ==============================================================  -->
          <!-- FORMULARIO-->
          <!--  ==============================================================  -->
        </form>
      </div>



    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- mapa -->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row">
    <div class="col-xs-12 top20 bottom20 padding0">
       <h2 class="titulo-linha-baixo">ONDE ESTAMOS</h2>

            <div class="col-xs-4 text-center top10">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-location.jpg" alt="">
            </div>

            <div class="col-xs-8 top25">
              <p ><?php Util::imprime($config[endereco]); ?></p>
            </div>
    </div> 
      <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="447" frameborder="0" style="border:0" allowfullscreen></iframe>
    
    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- mapa -->
  <!--  ==============================================================  -->


</div>
</div>
<!--  ==============================================================  -->
<!-- CONTATOS E FORMULARIO-->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('../includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>
