
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",14) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 164px no-repeat;
  }
</style>





<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- TITULO BG INTERNAS  -->
  <!-- ======================================================================= -->

  <div class="container alt_pag_internas">
    <div class="row">
      <div class="container_banner_faixa_internas"></div>
      <div class="col-xs-12 text-center top150">
       <div class="col-xs-offset-2  titulo_pagina_internas effect2">
         <h1 class="pt10">PRODUTOS</h1>
         <P>EM DESTAQUES</P>
       </div>
     </div>
   </div>
 </div>
 <!-- ======================================================================= -->
 <!-- TITULO BG INTERNAS  -->
 <!-- ======================================================================= -->



 <!--  ==============================================================  -->
 <!--barra categorias home-->
 <!--  ==============================================================  -->
 <div class="container">
   <div class="row barra_categorias">
     <div class="col-xs-12 ">
      <select name="select_categorias" class="selectpicker" data-max-options="1" data-live-search="true" title="TODAS AS NOSSAS CATEGORIAS" data-width="100%">
        <optgroup label="SELECIONE">
          <?php 
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while($row = mysql_fetch_array($result)){
            ?>
              <option value="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]); ?>" data-tokens="<?php Util::imprime($row[titulo]); ?>"><?php Util::imprime($row[titulo]); ?></option>
            <?php 
            }
          }
          ?>
        </optgroup>
      </select>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!--barra categorias home-->
<!--  ==============================================================  -->



 
<!--  ==============================================================  -->
<!--PRODUTOS HOME-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row produtos_home">


   <!--  ==============================================================  -->
   <!-- ITEM 01 -->
   <!--  ==============================================================  -->
   <?php 
   //  FILTRA AS CATEGORIAS
    if (isset( $_GET[get1] )) {
        $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $_GET[get1]);
        $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
    }


    //  FILTRA PELO TITULO
    if(isset($_POST[busca_produtos]) and !empty($_POST[busca_produtos]) ):
      $complemento = "AND titulo LIKE '%$_POST[busca_produtos]%'";
    endif;
      
    //  ORDENA PELO TITULO  
    $complemento .= "order by titulo";   

    $result = $obj_site->select("tb_produtos", $complemento);
    if(mysql_num_rows($result) == 0){
      echo "<p class='bg-danger top20' style='padding: 20px;'>Nenhum produto encontrado.</p>";
    }else{
      $i = 0;
    while($row = mysql_fetch_array($result)){
      ?>
        <div class="col-xs-6 top10" >
          <div class="thumbnail">
            <a href="<?php echo Util::caminho_projeto(); ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 205, 128, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
            </a>          
            <div class="caption">
            <h1><?php Util::imprime($row[titulo]); ?></h1>
            <div class="top10 bottom10">
              <a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn_produtos_home_geral pt5" role="button" title="Saiba mais">Saiba Mais</a> 

              <a class="btn btn-default btn_produtos_home_geral" role="button" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon_carrinho_home.png" alt="Solicite um orçamento">
              </a>
            </div>
          </div>
        </div>
      </div>
      <?php
      if ($i == 1) {
        echo '<div class="clearfix"></div>';
        $i = 0;
      }else{
        $i++;
      }
    }
  }
  ?>
    <!--  ==============================================================  -->
    <!-- ITEM 01 -->
    <!--  ==============================================================  -->



</div>
</div>
<!--  ==============================================================  -->
<!-- PRODUTOS HOME -->
<!--  ==============================================================  -->

<?php require_once('../includes/rodape.php'); ?>

</body>

</html>