


<!--  ==============================================================  -->
<!-- telefones topo-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">
    <!-- contatos -->
    <div class="col-xs-12 contatos_topo top5 bottom15 padding0">

     <div class="col-xs-5">
      <a href="<?php echo Util::caminho_projeto() ?>/mobile/">
        <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/logo.png" alt="">
      </a>
    </div>

    <div class="col-xs-7">
      <div class="pull-right">

        <div class="media ">
          <div class="media-left media-middle">
            <h4><?php Util::imprime($config[telefone1]); ?></h4>
          </div>
          <div class="media-body">
            <a class="btn btn_vermelho_topo left25" href="tel:+55<?php Util::imprime($config[telefone1]); ?>">CHAMAR</a>
          </div>
        </div>

        <?php if (!empty($config[telefone3])): ?>
          <div class="media">
            <div class="media-left media-middle">
              <h4><i class="fa fa-whatsapp" aria-hidden="true"></i><?php Util::imprime($config[telefone3]); ?></h4>
            </div>
            <div class="media-body">
              <a class="btn btn_vermelho_topo" href="tel:+55<?php Util::imprime($config[telefone3]); ?>">CHAMAR</a>
            </div>
          </div>
        <?php endif ?>
      </div>

    </div>

  </div>
  <!-- contatos -->
</div>
</div>
<!--  ==============================================================  -->
<!-- telefones topo-->
<!--  ==============================================================  -->



<!--  ==============================================================  -->
<!-- menu-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row menu-topo">
    <!--  ==============================================================  -->
    <!-- menu-->
    <!--  ==============================================================  -->
    <div class="col-xs-5 padding0">
      <div class=" dropdown top15">
        <a id="dLabel" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-menu">
          <i class="fa fa-bars right15"></i>
          NOSSO MENU
          <i class="fa fa-chevron-down left15"></i>
        </a>
        <ul class="dropdown-menu sub-menu" aria-labelledby="dLabel">
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/">HOME</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">EMPRESA</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos">SERVIÇOS</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">PRODUTOS</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas">DICAS</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/contatos">CONTATOS</a></li>
          <li><a href="<?php echo Util::caminho_projeto() ?>/mobile/trabalhe-conosco">TRABALHE CONOSCO</a></li>
        </ul>
      </div>
    </div>
    <!--  ==============================================================  -->
    <!-- menu-->
    <!--  ==============================================================  -->



    <!-- ======================================================================= -->
    <!-- PESQUISAS-->
    <!-- ======================================================================= -->
    <div class=" col-xs-2 dropdown text-center">
      <a href="#" class="dropdown-toggle btn btn_pesquisas_topo" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-search right10"></i>
        <span class="caret"></span></a>

        <ul class="dropdown-menu form_busca_topo">
          <form class="navbar-form navbar-left" action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
            <div class="form-group col-xs-8">
              <input type="text" class="form-control" placeholder="O que está procurando?" name="busca_produtos">
            </div>
            <button type="submit" class="btn btn_vermelho_topo">Buscar</button>
          </form>
        </ul>
      </div>
      <!-- ======================================================================= -->
      <!-- PESQUISAS-->
      <!-- ======================================================================= -->

      <!-- ======================================================================= -->
      <!-- botao carrinho de compra -->
      <!-- ======================================================================= -->
      <div class="col-xs-5 dropdown text-right padding0">
        <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento" class="btn btn_vermelho_carrinho" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
          <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/icon_carrinho.png" alt="">
        </a>

        <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

         <?php
         if(count($_SESSION[solicitacoes_produtos]) > 0)
         {
          for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
          {
            $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
            ?>
            <div class="lista-itens-carrinho col-xs-12">
              <div class="col-xs-2">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"")) ?>
              </div>
              <div class="col-xs-8">
                <h1><?php Util::imprime($row[titulo]) ?></h1>
              </div>
              <div class="col-xs-1">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
              </div>
            </div>
            <?php
          }
        }





        if(count($_SESSION[solicitacoes_servicos]) > 0)
        {


          for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++)
          {
            $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
            ?>
            <div class="lista-itens-carrinho col-xs-12">
              <div class="col-xs-2">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 46, 29, array("class"=>"", "alt"=>"")) ?>
              </div>
              <div class="col-xs-8">
                <h1><?php Util::imprime($row[titulo]) ?></h1>
              </div>
              <div class="col-xs-1">
                <a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamentos/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
              </div>
            </div>
            <?php
          }
        }

        ?>

        <div class="text-right top10 bottom20">
          <a class="btn btn_vermelho_topo" href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento" title="Finalizar" >
            FINALIZAR
          </a>
        </div>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- botao carrinho de compra -->
    <!-- ======================================================================= -->
  </div>
</div>
<!--  ==============================================================  -->
<!-- menu-->
<!--  ==============================================================  -->
