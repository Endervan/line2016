
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();


// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];




//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
    switch($_GET[tipo])
    {
        case "produto":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_produtos][$id]);
            sort($_SESSION[solicitacoes_produtos]);
        break;
        case "servico":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_servicos][$id]);
            sort($_SESSION[solicitacoes_servicos]);
        break;
        case "piscina_vinil":
            $id = $_GET[id];
            unset($_SESSION[piscina_vinil][$id]);
            sort($_SESSION[piscina_vinil]);
        break;
    }

}

?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 19) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 164px no-repeat;
  }
</style>





<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- TITULO BG INTERNAS  -->
  <!-- ======================================================================= -->

  <div class="container alt_pag_internas">
    <div class="row">
      <div class="container_banner_faixa_internas"></div>
      <div class="col-xs-12 text-center top205">
       <div class="col-xs-offset-2  titulo_pagina_internas effect2">
         <h1 class="pt15">ORÇAMENTO</h1>
       </div>
     </div>
   </div>
 </div>
 <!-- ======================================================================= -->
 <!-- TITULO BG INTERNAS  -->
 <!-- ======================================================================= -->




  

<?php
//  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
if(isset($_POST[nome])){

    //  CADASTRO OS PRODUTOS SOLICITADOS
    for($i=0; $i < count($_POST[qtd]); $i++){
        $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

        $mensagem .= "
                    <tr>
                        <td><p>". $dados[codigo] ."</p></td>
                        <td><p>". $_POST[qtd][$i] ."</p></td>
                        <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                     </tr>
                    ";
    }



    if (count($_POST[categorias]) > 0) {
        foreach($_POST[categorias] as $cat){
            $desc_cat .= $cat . ' , ';
        }
    }








    //  ENVIANDO A MENSAGEM PARA O CLIENTE
    $texto_mensagem = "
                        O seguinte cliente fez uma solicitação pelo site. <br />


                        Nome: $_POST[nome] <br />
                        Email: $_POST[email] <br />
                        Telefone: $_POST[telefone] <br />
                        Celular: $_POST[celular] <br />
                        Bairro: $_POST[bairro] <br />
                        Cidade: $_POST[cidade] <br />
                        Estado: $_POST[estado] <br />
                        Complemento: $_POST[complemento] <br />
                        Cep: $_POST[cep] <br />
                        Como conheceu: $_POST[como_conheceu] <br />


                        Mensagem: <br />
                        ". nl2br($_POST[mensagem]) ." <br />


                        <br />
                        <h2> Produtos selecionados:</h2> <br />

                        <table width='100%' border='0' cellpadding='5' cellspacing='5'>

                            <tr>
                                  <td><h4>CÓDIGO</h4></td>
                                  <td><h4>QTD</h4></td>
                                  <td><h4>ITEM</h4></td>
                            </tr>

                            $mensagem

                        </table>
                        ";


    Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
    Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
    unset($_SESSION[solicitacoes_produtos]);
    unset($_SESSION[solicitacoes_servicos]);
    unset($_SESSION[piscinas_vinil]);
    Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

}
?>




<form class="form-inline FormContato" role="form" method="post">

    
  <!--  ==============================================================  -->
  <!-- carrinho e formulario -->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row top60">

      <div class="col-xs-12 tb-lista-itens">
        <h5 class="bottom30">ITENS SELECIONADOS ( <?php echo count($_SESSION[solicitacoes_produtos]) ?> )</h5>
        <table class="table ">

          <tbody>
            <?php
              for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
              {
                $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                ?>
                <tr>
                  <td><img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" height="50" width="50" ></td>
                  <td><?php Util::imprime($row[titulo]) ?></td>
                  <td class="text-center">
                    <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                    <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                  </td>
                  <td class="text-center">
                    <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                      <i class="glyphicon glyphicon-remove top10 link-excluir"></i>
                    </a>
                  </td>
                </tr>
              <?php 
              }
              ?>
          </tbody>
        </table>

        


      </div>

      <div class="col-xs-12 formulario-orcamento bottom20">
        <h5 class=" bottom10">CONFIRME SEUS DADOS</h5>
      </div>


     <div class="formulario-orcamento bottom30 ">



      


        <div class="titulo-form-orcamento">

          <!-- form fale conosco -->
          <div class="col-xs-6 form-group">
            <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
            <input type="text" name="nome" class="form-control input100" placeholder="">
          </div>
          <div class="col-xs-6 form-group">
            <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
            <input type="text" name="email" class="form-control input100" placeholder="">
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
            <input type="text" name="telefone" class="form-control input100" placeholder="">
          </div>
          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Estado</span></label>
            <input type="text" name="estado" class="form-control input100" placeholder="">
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Cidade</span></label>
            <input type="text" name="cidade" class="form-control input100" placeholder="">
          </div>
          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Bairro</span></label>
            <input type="text" name="bairro" class="form-control input100" placeholder="">
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Complemento</span></label>
            <input type="text" name="complemento" class="form-control input100" placeholder="">
          </div>
          <div class="col-xs-6 top20 form-group">
            <label class="glyphicon glyphicon-map-marker"> <span>Cep</span></label>
            <input type="text" name="cep" class="form-control input100" placeholder="">
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-12 top20 form-group">
            <label class="glyphicon glyphicon-pencil"> <span>Mensagem</span></label>
            <textarea name="mensagem" id="" cols="30" rows="10" class="form-control input100"></textarea>
          </div>

          <div class="clearfix"></div>

          <!-- perguntas frequentes -->
          <div class=" col-xs-12 top30 form-group">
            <p>Tem interesse em receber orçamento de outros produtos:</p>

            <label class="radio-inline">
              <input type="radio" name="receber_orcamento" id="inlineRadio1" value="option1"> SIM
            </label>
            <label class="radio-inline">
              <input type="radio" name="receber_orcamento" id="inlineRadio2" value="option2"> NÃO
            </label>  
        </div>

        <div>
            <div class="top25">
              <p>Se sim, assinale abaixo os produtos</p>
            </div>

            <?php
              $result = $obj_site->select("tb_categorias_produtos", "LIMIT 4");
              if(mysql_num_rows($result) > 0)
              {
                while($row = mysql_fetch_array($result))
                {
                ?>
                <div class="checkbox col-xs-12">
                  <label>
                    <input type="checkbox" name="categorias[]" value="<?php Util::imprime($row[titulo]) ?>">
                    <?php Util::imprime($row[titulo]) ?>
                  </label>
                </div>
                <?php
                }
            }
            ?>


          </div>
          <!-- perguntas frequentes -->


          <div class="pesquisas">
            <div class=" col-xs-12 top20 form-group">
                 <p class="bottom10">Como você conheceu o nosso site?</p>
                <select class="form-control" name="como_conheceu">
                <option value="">SELECIONE</option>
                <option>GOOGLE</option>
                <option>INTERNET</option>
                <option>JORNAIS</option>
                <option>REVISTAS</option>
                <option>REDE SOCIAIS</option>
                <option>OUTROS</option>
              </select>
            </div>       

            <div class="col-xs-12 top10 bottom40 mobile">
              <button type="submit" class="btn btn-default btn-formulario pull-right">ENVIAR</button>
            </div>

          </div>

          <!-- form fale conosco -->
        </div>
      

    </div>



  </div>
</div>
<!--  ==============================================================  -->
<!-- carrinho e formulario -->
<!--  ==============================================================  -->


</form>







<?php require_once('../includes/rodape.php'); ?>

</body>

</html>





<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },

      receber_orcamento: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },

      
      como_conheceu: {
        validators: {
          notEmpty: {

          }
        }
      },


      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      fax: {
        validators: {
          notEmpty: {

          }
        }
      },
      tipoPessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      bairro: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>




