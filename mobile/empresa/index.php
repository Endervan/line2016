
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
	<?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 164px no-repeat;
  }
</style>

<body class="bg-interna">
	<!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!-- bg-empresa -->
  <!--  ==============================================================  -->
  <div class="container alt_pag_internas">
    <div class="row">
      <div class="container_banner_faixa_internas"></div>
      <div class="col-xs-10 titulo_paginas top80">
        <h1>CONHEÇA MAIS A</h1>
        <h1><span>LINE MATERIAIS ELÉTRICOS</span></h1>

      </div>
      <div class="col-xs-12 top50">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
        <div class="desc-mais-line">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>

      </div>
    </div>
    <!--  ==============================================================  -->
    <!-- bg-empresa -->
    <!--  ==============================================================  -->

   
  <!-- ======================================================================= -->
  <!-- atendimento e grupo  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top40 atendimento">
      
      
      <!-- grupo line -->
      <div class="col-xs-12 top50">
          <div class="border-empresa-grupo">
            <div class="">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-grupo-line-eletrica.jpg" alt="" class="logo-grupo" width="132" >
            </div>
            
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
            <p class="top10 bottom10 pg10"><?php Util::imprime($row[descricao]); ?></p>
          </div>
      </div>
      <!-- grupo line -->


      <!-- grupo engenharia -->
      <div class="col-xs-12 top50">
          <div class="border-empresa-grupo">
            <div class="">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-grupo-line-engenharia.jpg" alt="" class="logo-grupo" width="132" >
            </div>
            
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
            <p class="top10 bottom10 pg10"><?php Util::imprime($row[descricao]); ?></p>
          </div>
      </div>
      <!-- grupo engenharia -->


      <!-- grupo ar condicionado -->
      <div class="col-xs-12 top50">
          <div class="border-empresa-grupo">
            <div class="">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/loo-line-ar-condicionado.jpg" alt="" class="logo-grupo" width="132" >
            </div>
            
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6) ?>
            <p class="top10 bottom10 pg10"><?php Util::imprime($row[descricao]); ?></p>
          </div>
      </div>
      <!-- grupo ar condicionado -->

      <div class="col-xs-12 top50">
            <?php //require_once("./includes/contato.php") ?>
      </div>



    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- atendimento e grupo  -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- MAPS  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row relative top35">
      <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- MAPS  -->
  <!-- ======================================================================= -->


<?php require_once('../includes/rodape.php'); ?>

</body>

</html>