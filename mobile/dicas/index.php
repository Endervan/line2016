
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html>

<head>
  <?php require_once('.././includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",11) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 164px no-repeat;
  }
</style>





<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('../includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- TITULO BG INTERNAS  -->
  <!-- ======================================================================= -->

  <div class="container alt_pag_internas">
    <div class="row">
      <div class="container_banner_faixa_internas"></div>
      <div class="col-xs-12 text-center top180">
       <div class="col-xs-offset-2  titulo_pagina_internas effect2">
         <h1 class="pt15">NOSSAS DICAS</h1>
       </div>
     </div>
   </div>
 </div>
 <!-- ======================================================================= -->
 <!-- TITULO BG INTERNAS  -->
 <!-- ======================================================================= -->

 <!--  ==============================================================  -->
 <!--DICAS DESCRICAO-->
 <!--  ==============================================================  -->
 <div class="container">
  <div class="row">
      <!--  ==============================================================  -->
      <!--DICAS 01-->
      <!--  ==============================================================  -->
      <?php
      $result = $obj_site->select("tb_dicas");
      if(mysql_num_rows($result) > 0){
       while ($row = mysql_fetch_array($result)) {
         ?> 

         <div class="col-xs-12 dicas_desc top25">
          <div class="thumbnail">
            <div class="top15 bottom10">
              <h1><?php Util::imprime($row[titulo]); ?></h1>
            </div>
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
             <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",381, 214, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
           </a>
           <div class="caption top10 text-right">
            <p><?php Util::imprime($row[descricao],300); ?></p>
            <div class="top10 bottom10">
              <a class="btn btn_dicas_saiba" href="<?php echo Util::caminho_projeto() ?>/mobile/dica/<?php Util::imprime($row[url_amigavel]); ?>">
                Saiba Mais
              </a> 
            </div>
          </div>
        </div>
      </div>
      <?php 
      if ($i == 1) {
        echo '<div class="clearfix"></div>';
        $i = 0;
      }else{
        $i++;
      }

    }
  }
  ?>

</div>
</div>
<!--  ==============================================================  -->
<!--DICAS DESCRICAO-->
<!--  ==============================================================  -->



<?php require_once('../includes/rodape.php'); ?>

</body>

</html>