-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: 179.188.16.39
-- Generation Time: 16-Maio-2016 às 16:08
-- Versão do servidor: 5.6.21-69.0-log
-- PHP Version: 5.6.20-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `line2016`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners`
--

CREATE TABLE `tb_banners` (
  `idbanner` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ativo` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `tipo_banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `legenda` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url_btn_orcamento` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_banners`
--

INSERT INTO `tb_banners` (`idbanner`, `titulo`, `imagem`, `ativo`, `ordem`, `tipo_banner`, `url_amigavel`, `url`, `legenda`, `url_btn_orcamento`) VALUES
(1, 'CONHEÇA NOSSA', '1205201601041395513647..jpg', 'SIM', NULL, '2', 'conheca-nossa', '', 'PROMOÇÃO DE LÂMPADAS', NULL),
(2, 'CONHEÇA NOSSA', '1205201601061296981346..jpg', 'SIM', NULL, '2', 'conheca-nossa', '/mobile/produtos', 'PROMOÇÃO DE LÂMPADAS 01', NULL),
(3, 'CONHEÇA NOSSA', '1005201609121301515657..jpg', 'SIM', NULL, '1', 'conheca-nossa', '/produtos', 'PROMOÇÃO DE LÂMPADAS', NULL),
(4, 'CONHEÇA NOSSA', '1005201609131113802502..jpg', 'SIM', NULL, '1', 'conheca-nossa', '/produtos', 'PROMOÇÃO DE LÂMPADAS 01', NULL),
(5, 'Produtos Lateral 1', '1305201610031131371789..jpg', 'SIM', NULL, '3', 'produtos-lateral-1', '/produtos', '', NULL),
(6, 'Produtos Lateral 1', '1305201610031403122664..jpg', 'SIM', NULL, '3', 'produtos-lateral-1', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_banners_internas`
--

CREATE TABLE `tb_banners_internas` (
  `idbannerinterna` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `legenda` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_banners_internas`
--

INSERT INTO `tb_banners_internas` (`idbannerinterna`, `titulo`, `imagem`, `ordem`, `url_amigavel`, `ativo`, `legenda`) VALUES
(1, 'Empresa', '1005201609281266056953.jpg', NULL, 'empresa', 'SIM', NULL),
(2, 'Dicas', '1605201611221193458568.jpg', NULL, 'dicas', 'SIM', NULL),
(3, 'Serviços', '1105201612401346485037.jpg', NULL, 'servicos', 'SIM', NULL),
(4, 'Serviço dentro', '1105201601341115322705.jpg', NULL, 'servico-dentro', 'SIM', NULL),
(5, 'Produtos', '1105201610411249831816.jpg', NULL, 'produtos', 'SIM', NULL),
(6, 'Produtos dentro', '1105201611231287401817.jpg', NULL, 'produtos-dentro', 'SIM', NULL),
(7, 'Orçamento', '1105201602001145679526.jpg', NULL, 'orcamento', 'SIM', NULL),
(8, 'Contatos', '1105201603161261117414.jpg', NULL, 'contatos', 'SIM', NULL),
(9, 'Trabalhe Conosco', '1105201604481292337556.jpg', NULL, 'trabalhe-conosco', 'SIM', NULL),
(10, 'Mobile Empresa', '1605201609591198813981.jpg', NULL, 'mobile-empresa', 'SIM', NULL),
(11, 'Mobile Dicas', '1605201611181277867905.jpg', NULL, 'mobile-dicas', 'SIM', NULL),
(12, 'Mobile Contatos', '1305201611281130917927.jpg', NULL, 'mobile-contatos', 'SIM', NULL),
(13, 'MObile Trabalhe Conosco', '1305201601031228026983.jpg', NULL, 'mobile-trabalhe-conosco', 'SIM', NULL),
(14, 'Mobile Produtos', '1305201603221275988493.jpg', NULL, 'mobile-produtos', 'SIM', NULL),
(15, 'Mobile produto dentro', '1305201603541146007967.jpg', NULL, 'mobile-produto-dentro', 'SIM', NULL),
(16, 'Mobile Serviços', '1305201606021398004324.jpg', NULL, 'mobile-servicos', 'SIM', NULL),
(17, 'Mobile Serviços Dentro', '1305201606321391015829.jpg', NULL, 'mobile-servicos-dentro', 'SIM', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `idcategoriaproduto_pai` int(11) DEFAULT NULL,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `idcategoriaproduto_pai`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`) VALUES
(75, 'INTERRUPTORES E TOMADAS', NULL, '1105201606091357880863.png', 'SIM', NULL, 'interruptores-e-tomadas', '', '', ''),
(74, 'FIOS E CABOS', NULL, '1105201606091357880863.png', 'SIM', NULL, 'fios-e-cabos', '', '', ''),
(76, 'INFRAESTRUTURA', NULL, '1105201606091357880863.png', 'SIM', NULL, 'infraestrutura', '', '', ''),
(77, 'AUTOMAÇÃO', NULL, '1105201606091357880863.png', 'SIM', NULL, 'automacao', '', '', ''),
(78, 'CATEGORIA 1', NULL, '1105201606091357880863.png', 'SIM', NULL, 'categoria-1', NULL, NULL, NULL),
(79, 'CATEGORIA 2', NULL, '1105201606091357880863.png', 'SIM', NULL, 'categoria-2', NULL, NULL, NULL),
(80, 'CATEGORIA 3', NULL, '1105201606091357880863.png', 'SIM', NULL, 'categoria-3', NULL, NULL, NULL),
(81, 'CATEGORIA 4', NULL, '1105201606091357880863.png', 'SIM', NULL, 'categoria-3', NULL, NULL, NULL),
(82, 'CATEGORIA 5', 81, '1105201606091357880863.png', 'SIM', NULL, 'categoria-4', '', '', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE `tb_configuracoes` (
  `idconfiguracao` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `google_plus` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `google_plus`, `telefone3`, `telefone4`) VALUES
(1, '', '', '', 'SIM', 0, '', '2ª Avenida, Bloco 241, Loja 1 - Núcleo Bandeirante, Brasília - DF', '(61)1111-1111', '(61)2222-2222', 'marciomas@gmail.com', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d122829.18590934838!2d-48.11783072819571!3d-15.834925803033512!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a32f62027f279%3A0x9a336abb283db55a!2sExtra+Hiper!5e0!3m2!1spt-BR!2sbr!4v146222093908', NULL, NULL, '', 'https://www.google.com/', '(61)3333-3333', '(61)4444-44444');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` longtext CHARACTER SET utf8,
  `keywords_google` longtext CHARACTER SET utf8,
  `description_google` longtext CHARACTER SET utf8,
  `data` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(36, 'Dica 1 Lorem ipsum dolor sit amet lorem Lorem ipsum dolor sit amet loremLorem ipsum dolor sit amet loremLorem ipsum dolor sit amet lorem', '<p>\r\n	teste Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS &nbsp;com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '1105201606281279765564..jpg', 'SIM', NULL, 'dica-1-lorem-ipsum-dolor-sit-amet-lorem-lorem-ipsum-dolor-sit-amet-loremlorem-ipsum-dolor-sit-amet-loremlorem-ipsum-dolor-sit-amet-lorem', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', 'Dica 1 Lorem ipsum dolor sit amet', NULL),
(37, 'Dica 2 Lorem ipsum dolor sit amet', '<div>\r\n	1 - Constru&ccedil;&atilde;o de piscina &eacute; coisa s&eacute;ria e cara. Economize na escolha dos materiais, mas n&atilde;o na m&atilde;o de obra.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	2 - Defina qual o tamanho da piscina que ir&aacute; escolher. Se pretende reunir a fam&iacute;lia e muitos amigos evite piscinas pequenas.</div>\r\n<div>\r\n	&nbsp;&nbsp;</div>\r\n<div>\r\n	3 - Se voc&ecirc; tem ou pretende ter crian&ccedil;as e animais, escolha uma piscina que n&atilde;o seja muito funda e se preocupe com a constru&ccedil;&atilde;o de barreiras e coberturas, por quest&otilde;es de seguran&ccedil;a. As medidas mais usadas s&atilde;o de at&eacute; 1,30m ~ 1,40 na parte mais funda e 0,40m ~ 0,50m na parte mais rasa.</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	4 - Escolha um local com boa incid&ecirc;ncia de sol. Ningu&eacute;m quer usar piscina que fica na sombra!</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	5 - Evite a constru&ccedil;&atilde;o da piscina em locais com muitas &aacute;rvores, al&eacute;m de fazerem sombra, as folhas podem tornar a limpeza e manuten&ccedil;&atilde;o da piscina um tormento.</div>', '1205201609371305657663..jpg', 'SIM', NULL, 'dica-2-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(38, 'Dica 3 Lorem ipsum dolor sit amet', '<p>\r\n	teste Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS &nbsp;com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '1205201609391337452576..jpg', 'SIM', NULL, 'dica-3-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(39, 'Dica 4 Lorem ipsum dolor sit amet', '<p>\r\n	teste Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p>\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>\r\n<p>\r\n	Terminada a caixa de alvenaria chega a hora da instala&ccedil;&atilde;o do bols&atilde;o. Contrate pessoal t&eacute;cnico para a esta fase, para que n&atilde;o ocorram rugas e outros defeitos na instala&ccedil;&atilde;o. Aqui ocorre a instala&ccedil;&atilde;o de todos os acess&oacute;rios e equipamentos, inclusive filtros e bombas para o posterior enchimento de &aacute;gua da mesma e utiliza&ccedil;&atilde;o.</p>\r\n<p>\r\n	Nas piscinas de vinil deve-se ter cuidado na utiliza&ccedil;&atilde;o da mesma, observe ou alerte para n&atilde;o se utiliz&aacute;-la com objetos pontiagudos ou cortantes, para n&atilde;o perfurar ou rasgar o vinil. Assim ter&aacute; vida longa para sua piscina.</p>\r\n<p>\r\n	Em casos de furos ou rasgos podem-se fazer REMENDOS &nbsp;com vinil e cola e o funcionamento n&atilde;o ser&aacute; alterado.</p>', '1205201609411376035271..jpg', 'SIM', NULL, 'dica-4-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(40, 'Dica 5 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '1205201609441123509758..jpg', 'SIM', NULL, 'dica-5-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(41, 'Dica 6 Lorem ipsum dolor sit amet', '<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Definido o tamanho escolhido para a constru&ccedil;&atilde;o &eacute; necess&aacute;rio a contrata&ccedil;&atilde;o de uma empresa para fazer a escava&ccedil;&atilde;o da caixa ou buraco.</p>\r\n<p style="box-sizing: border-box; margin: 0px auto; padding: 0px; list-style: none; outline: none; border: none; font-size: 20px; color: rgb(0, 0, 0); text-align: justify; font-family: Lato, sans-serif; line-height: 28.5714px;">\r\n	Ap&oacute;s a caixa aberta, um construtor de sua confian&ccedil;a ou Empresa contratada far&aacute; a parte de alvenaria, construindo a caixa propriamente dita, a alvenaria deve ter acabamento no fundo e nas laterais de argamassa, pois o vinil ficar&aacute; assentado nesta caixa e em contato direto com as paredes e fundo, o acabamento em argamassa evita o surgimento de micro furos por for&ccedil;a do atrito do vinil com as paredes e fundo da piscina, sendo que no fundo utiliza-se uma manta anti-impacto, at&eacute; porque &eacute; a parte onde a press&atilde;o ser&aacute; maior ap&oacute;s a mesma estar pronta e cheia de &aacute;gua.</p>', '1205201609461377203061..jpg', 'SIM', NULL, 'dica-6-lorem-ipsum-dolor-sit-amet', '', '', '', NULL),
(42, 'Dicas Mobile Lorem ipsum dolor sit amet, consetetur', '<p>\r\n	<span style="color: rgb(0, 0, 0); font-family: Lato, sans-serif; font-size: 20px; line-height: 28.5714px; text-align: justify; background-color: rgb(243, 221, 146);">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</span></p>', '1305201609401354102710..jpg', 'SIM', NULL, 'dicas-mobile-lorem-ipsum-dolor-sit-amet-consetetur', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE `tb_empresa` (
  `idempresa` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'Index - Nosso serviços', 'Lorem Ipsum is simply dummy text of the p rinting and typesetting industry. Lorem Ipsu m has been the industry\'s standard dummy text ever since the 1500s', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(2, 'Index - Sobre a Line', 'Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impres sor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, co mo também ao salto para a editoração eletrônica, permanecendo essencialme nte inalterado. Se popularizou na década', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(3, 'Empresa - Conheça mais a Line Elétricos', 'Lorem Ipsum is simply dummy text of the printing and typesetting industr\rum has been the industry\'s standard dummy text ever since the 1500sw\rown printer took a galley of type and scrambled it to make a type specim\ras survived not only five centuries, but also the leap into electronic types\rning essentially unchanged. It was popularised in the 1960s with the rele\rset sheets containing Lorem Ipsum passages, and more recently with des\ring software like Aldus PageMaker including versions of Lorem Ips', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(4, 'Empresa - Grupo Line', 'Lorem Ipsum is simply dummy\rtext of the printing and types\retting industry. Lorem Ipsum \rhas been the industry\'s stand\rard dummy text ever since th\re 1500swhen an unknown p\rrinter took a galley of type an\rd scrambled it to make a typ\re specimen book. It has survi\rved not only five centuries, b\rut also the leap into electron\ric typesetting, remaining esse\rntially unchanged. It was po\rpularised in the 1960s with\r the release of Letra', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(5, 'Empresa - Line Engenharia', 'Lorem Ipsum is simply dummy\rtext of the printing and types\retting industry. Lorem Ipsum \rhas been the industry\'s stand\rard dummy text ever since th\re 1500swhen an unknown p\rrinter took a galley of type an\rd scrambled it to make a typ\re specimen book. It has survi\rved not only five centuries, b\rut also the leap into electron\ric typesetting, remaining esse\rntially unchanged. It was po\rpularised in the 1960s with\r the release of Letra', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(6, 'Empresa - Line ar condicionado', 'Lorem Ipsum is simply dummy\rtext of the printing and types\retting industry. Lorem Ipsum \rhas been the industry\'s stand\rard dummy text ever since th\re 1500swhen an unknown p\rrinter took a galley of type an\rd scrambled it to make a typ\re specimen book. It has survi\rved not only five centuries, b\rut also the leap into electron\ric typesetting, remaining esse\rntially unchanged. It was po\rpularised in the 1960s with\r the release of Letra', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(7, 'Serviço - Descrição', 'Serviço - Descrição Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy te\rxt ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survi\rved not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1\r960s with the release of Letraset sheets containing Lorem Ipsum passages,', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(8, 'Index - OS NOSSOS SERVIÇOS', 'Index Lorem Ipsum is simply dummy text of the p rinting and typesetting industry. Lorem Ipsu m has been the industry\'s standard dummy text ever since the 1500s', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(9, 'Index - SOBRE A LINE MATERIAS ELÉTRICO', 'Sobre a Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impres sor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, co mo também ao salto para a editoração eletrônica, permanecendo essencialme nte inalterado. Se popularizou na década', 'SIM', 0, '', '', '', '', NULL, NULL, NULL),
(10, 'Index - AS NOSSAS DICAS', 'Nossa dicas Lorem Ipsum is simply dummy text of the p\n                rinting and typesetting industry. ', 'SIM', 0, '', '', '', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_equipamentos`
--

CREATE TABLE `tb_equipamentos` (
  `idequipamento` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) UNSIGNED NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `id_subcategoriaproduto` int(11) DEFAULT NULL,
  `imagem_interna` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_equipamentos`
--

INSERT INTO `tb_equipamentos` (`idequipamento`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `id_subcategoriaproduto`, `imagem_interna`) VALUES
(1, 'Equpamento 1 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Produto 1&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Fogos de Artifícios 01', 'Fogos de Artifícios 01', 'Fogos de Artifícios 01', 'SIM', 0, 'equpamento-1-lorem-ipsum-dolor-sit-amet', 80, '3M', NULL, NULL, '', '', 1, '0405201610431245857231..jpg'),
(2, 'Equpamento 2 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Porta postigo direita Viena Ouro&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</p>\r\n<p>\r\n	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Fogos de Artifícios 02', 'Fogos de Artifícios 02', 'Fogos de Artifícios 02', 'SIM', 0, 'produto-2-lorem-ipsum-dolor-sit-amet', 77, 'Tigre', NULL, NULL, '', '', 3, '0405201610431245857231..jpg'),
(7, 'Equpamento 3 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Fogos de Artif&iacute;cios 0&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Fogos de Artifícios 03', 'Fogos de Artifícios 03', 'Fogos de Artifícios 03', 'SIM', 0, 'produto-3-lorem-ipsum-dolor-sit-amet', 82, 'Amanco', NULL, NULL, '', '', 4, '0405201610431245857231..jpg'),
(8, 'Equpamento 4 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	4&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-4-lorem-ipsum-dolor-sit-amet', 80, 'Sol', NULL, NULL, '', '', 1, '0405201610431245857231..jpg'),
(9, 'Equpamento 5 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'SIM', 0, 'produto-5-lorem-ipsum-dolor-sit-amet', 81, NULL, NULL, NULL, '', '', NULL, '0405201610431245857231..jpg'),
(10, 'Equpamento 6 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt</p>', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'SIM', 0, 'produto-6-lorem-ipsum-dolor-sit-amet', 77, NULL, NULL, NULL, '', '', NULL, '0405201610431245857231..jpg'),
(11, 'Equpamento 7 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-7-lorem-ipsum-dolor-sit-amet', 77, NULL, NULL, NULL, 'https://www.youtube.com/embed/xIM22tZAdwE', '<p>\r\n	Apresenta&ccedil;&atilde;o do v&iacute;deo Lorem Ipsum is simply dummy text of the printing and typesettin g industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a gall ey of type and scrambled it to make a type specimen book. It h as survived not only five centuries, but alsothe leap into electro nic typesetting, remaining essentially unchanged. It was popularis ed in the 1960</p>', NULL, '0405201610431245857231..jpg'),
(12, 'Equpamento 8 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Produto 8 Lorem ipsum dolor sit amet&nbsp;Produto 8 Lorem ipsum dolor sit amet&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-8-lorem-ipsum-dolor-sit-amet', 75, NULL, NULL, NULL, '', '', NULL, '0405201610431245857231..jpg'),
(13, 'Equpamento 9 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-9-lorem-ipsum-dolor-sit-amet', 74, NULL, NULL, NULL, '', '', NULL, '0405201610431245857231..jpg'),
(14, 'Equpamento 1001 Lorem ipsum dolor sit amet', '0405201601491120218923.png', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-1001-lorem-ipsum-dolor-sit-amet', 82, NULL, NULL, NULL, '', '', 4, '0405201610431245857231..jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(1, '2304201608391132287293.jpg', 'SIM', NULL, NULL, 1),
(2, '2304201608391390468664.jpeg', 'SIM', NULL, NULL, 1),
(3, '2304201608391216243444.jpg', 'SIM', NULL, NULL, 1),
(4, '2304201608391286373039.jpg', 'SIM', NULL, NULL, 1),
(5, '2304201608391196362761.jpg', 'SIM', NULL, NULL, 1),
(6, '2304201608391257217307.jpeg', 'SIM', NULL, NULL, 1),
(7, '2304201608401214716216.jpg', 'SIM', NULL, NULL, 1),
(10, '2304201608401291559253.jpg', 'SIM', NULL, NULL, 1),
(11, '2304201608401328922935.jpg', 'SIM', NULL, NULL, 2),
(12, '2304201608401121401425.jpeg', 'SIM', NULL, NULL, 2),
(13, '2304201608401326053386.jpg', 'SIM', NULL, NULL, 7),
(14, '2304201608401266971012.jpeg', 'SIM', NULL, NULL, 7),
(15, '2304201608401342028212.jpg', 'SIM', NULL, NULL, 2),
(16, '2304201608401334137434.jpg', 'SIM', NULL, NULL, 7),
(17, '2304201608401299135454.jpg', 'SIM', NULL, NULL, 2),
(18, '2304201608401301585086.jpg', 'SIM', NULL, NULL, 7),
(19, '2304201608401164767362.jpg', 'SIM', NULL, NULL, 8),
(20, '2304201608401136114268.jpeg', 'SIM', NULL, NULL, 8),
(21, '2304201608401113833066.jpg', 'SIM', NULL, NULL, 2),
(22, '2304201608401256848592.jpg', 'SIM', NULL, NULL, 7),
(23, '2304201608401397319780.jpg', 'SIM', NULL, NULL, 8),
(24, '2304201608401170596735.jpeg', 'SIM', NULL, NULL, 2),
(25, '2304201608401398902206.jpeg', 'SIM', NULL, NULL, 7),
(26, '2304201608401351779362.jpg', 'SIM', NULL, NULL, 9),
(27, '2304201608401361983171.jpeg', 'SIM', NULL, NULL, 9),
(28, '2304201608401195748917.jpg', 'SIM', NULL, NULL, 8),
(29, '2304201608401343919274.jpg', 'SIM', NULL, NULL, 7),
(30, '2304201608401297893129.jpg', 'SIM', NULL, NULL, 2),
(31, '2304201608401299814369.jpg', 'SIM', NULL, NULL, 9),
(32, '2304201608401322948267.jpg', 'SIM', NULL, NULL, 7),
(33, '2304201608401350785028.jpg', 'SIM', NULL, NULL, 8),
(34, '2304201608401311382441.jpg', 'SIM', NULL, NULL, 2),
(35, '2304201608401400787798.jpg', 'SIM', NULL, NULL, 10),
(36, '2304201608401198274153.jpeg', 'SIM', NULL, NULL, 10),
(37, '2304201608401336804703.jpg', 'SIM', NULL, NULL, 9),
(38, '2304201608401359217121.jpeg', 'SIM', NULL, NULL, 8),
(39, '2304201608401168835486.jpg', 'SIM', NULL, NULL, 10),
(40, '2304201608411310709541.jpg', 'SIM', NULL, NULL, 8),
(41, '2304201608411368046935.jpg', 'SIM', NULL, NULL, 9),
(42, '2304201608411201570478.jpg', 'SIM', NULL, NULL, 11),
(43, '2304201608411120085087.jpeg', 'SIM', NULL, NULL, 11),
(44, '2304201608411250978247.jpg', 'SIM', NULL, NULL, 10),
(45, '2304201608411185044661.jpg', 'SIM', NULL, NULL, 8),
(46, '2304201608411270384475.jpeg', 'SIM', NULL, NULL, 9),
(47, '2304201608411255545885.jpg', 'SIM', NULL, NULL, 11),
(48, '2304201608411390762690.jpg', 'SIM', NULL, NULL, 10),
(49, '2304201608411129692391.jpg', 'SIM', NULL, NULL, 12),
(50, '2304201608411128176896.jpg', 'SIM', NULL, NULL, 9),
(51, '2304201608411232450966.jpeg', 'SIM', NULL, NULL, 12),
(52, '2304201608411112979030.jpg', 'SIM', NULL, NULL, 11),
(53, '2304201608411325840611.jpg', 'SIM', NULL, NULL, 9),
(54, '2304201608411246546794.jpeg', 'SIM', NULL, NULL, 10),
(55, '2304201608411182130584.jpg', 'SIM', NULL, NULL, 12),
(56, '2304201608411160079773.jpg', 'SIM', NULL, NULL, 10),
(57, '2304201608411286189121.jpg', 'SIM', NULL, NULL, 11),
(58, '2304201608411120380868.jpg', 'SIM', NULL, NULL, 13),
(59, '2304201608411308726899.jpeg', 'SIM', NULL, NULL, 13),
(60, '2304201608411219402158.jpg', 'SIM', NULL, NULL, 12),
(61, '2304201608411153269183.jpg', 'SIM', NULL, NULL, 10),
(62, '2304201608411114718367.jpeg', 'SIM', NULL, NULL, 11),
(63, '2304201608411138369015.jpg', 'SIM', NULL, NULL, 13),
(64, '2304201608411223154482.jpg', 'SIM', NULL, NULL, 11),
(65, '2304201608411177957868.jpg', 'SIM', NULL, NULL, 12),
(66, '2304201608411365556899.jpg', 'SIM', NULL, NULL, 14),
(67, '2304201608411293708919.jpg', 'SIM', NULL, NULL, 13),
(68, '2304201608411208373012.jpeg', 'SIM', NULL, NULL, 14),
(69, '2304201608411278302664.jpeg', 'SIM', NULL, NULL, 12),
(70, '2304201608411347220644.jpg', 'SIM', NULL, NULL, 11),
(71, '2304201608411216557624.jpg', 'SIM', NULL, NULL, 13),
(72, '2304201608411249469931.jpg', 'SIM', NULL, NULL, 12),
(73, '2304201608411165591436.jpg', 'SIM', NULL, NULL, 14),
(74, '2304201608411141598748.jpeg', 'SIM', NULL, NULL, 13),
(75, '2304201608411329651229.jpg', 'SIM', NULL, NULL, 14),
(76, '2304201608411394875887.jpg', 'SIM', NULL, NULL, 12),
(77, '2304201608411247300164.jpg', 'SIM', NULL, NULL, 13),
(78, '2304201608411306615434.jpg', 'SIM', NULL, NULL, 14),
(79, '2304201608411286060988.jpg', 'SIM', NULL, NULL, 13),
(80, '2304201608411400061668.jpeg', 'SIM', NULL, NULL, 14),
(81, '2304201608411291974774.jpg', 'SIM', NULL, NULL, 14),
(82, '2304201608411261315228.jpg', 'SIM', NULL, NULL, 14);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acesso_tags` varchar(3) DEFAULT 'NAO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`, `acesso_tags`) VALUES
(1, 'HomeWeb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br', 'SIM');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:00:35', 1),
(2, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:04:48', 1),
(3, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:07:05', 1),
(4, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:08:09', 1),
(5, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:29:16', 1),
(6, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '20:36:51', 1),
(7, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-02', '23:43:19', 1),
(8, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-03', '00:26:52', 1),
(9, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-03', '12:55:52', 1),
(10, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '13:49:39', 1),
(11, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '22:40:32', 1),
(12, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '22:43:25', 1),
(13, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '23:23:17', 1),
(14, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '23:23:48', 1),
(15, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '20:37:32', 1),
(16, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '23:48:58', 1),
(17, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-04', '23:53:28', 1),
(18, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:09:54', 1),
(19, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:10:17', 1),
(20, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:10:38', 1),
(21, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:16:08', 1),
(22, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '00:17:58', 1),
(23, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '11:47:16', 1),
(24, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '15:03:28', 1),
(25, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-05', '20:53:17', 1),
(26, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '12:39:07', 1),
(27, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:04:11', 1),
(28, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:06:54', 1),
(29, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:16:16', 1),
(30, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:16:48', 1),
(31, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:17:20', 1),
(32, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '16:19:58', 1),
(33, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '16:20:41', 1),
(34, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '16:26:01', 1),
(35, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '13:28:44', 1),
(36, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '21:46:41', 1),
(37, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-07', '23:10:36', 1),
(38, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:12:58', 1),
(39, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:13:37', 1),
(40, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:16:09', 1),
(41, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:27:51', 1),
(42, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:30:13', 1),
(43, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:31:16', 1),
(44, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:33:18', 1),
(45, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:35:55', 1),
(46, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '09:36:18', 1),
(47, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '21:28:19', 1),
(48, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-10', '20:26:00', 1),
(49, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '08:10:59', 1),
(50, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '12:36:13', 1),
(51, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '12:40:40', 1),
(52, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '13:32:27', 1),
(53, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '13:34:19', 1),
(54, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '10:41:41', 1),
(55, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '11:23:33', 1),
(56, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '14:00:50', 1),
(57, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '18:09:41', 1),
(58, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '15:16:27', 1),
(59, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '18:17:35', 1),
(60, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '18:17:52', 1),
(61, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '18:18:07', 1),
(62, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '16:48:56', 1),
(63, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '18:28:52', 1),
(64, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-11', '18:51:52', 1),
(65, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '09:37:13', 1),
(66, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '09:39:25', 1),
(67, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '09:41:48', 1),
(68, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '09:44:19', 1),
(69, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '09:46:49', 1),
(70, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:04:18', 1),
(71, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:06:47', 1),
(72, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:24:44', 1),
(73, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '13:27:26', 1),
(74, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-12', '20:48:31', 1),
(75, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '09:10:14', 1),
(76, 'CADASTRO DO CLIENTE ', '', '2016-05-13', '09:40:08', 1),
(77, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '09:41:18', 1),
(78, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '11:28:48', 1),
(79, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '13:03:01', 1),
(80, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '15:22:36', 1),
(81, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '15:54:56', 1),
(82, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '18:02:52', 1),
(83, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '18:32:22', 1),
(84, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '21:40:29', 4),
(85, 'CADASTRO DO CLIENTE ', '', '2016-05-13', '22:03:28', 4),
(86, 'CADASTRO DO CLIENTE ', '', '2016-05-13', '22:03:59', 4),
(87, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-13', '22:25:04', 4),
(88, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-14', '15:45:43', 4),
(89, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '09:59:39', 1),
(90, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '11:17:44', 1),
(91, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '11:18:25', 1),
(92, 'ALTERAÇÃO DO CLIENTE ', '', '2016-05-16', '11:22:24', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE `tb_produtos` (
  `idproduto` int(10) UNSIGNED NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` longtext CHARACTER SET utf8 NOT NULL,
  `keywords_google` longtext CHARACTER SET utf8 NOT NULL,
  `description_google` longtext CHARACTER SET utf8 NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) UNSIGNED NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) UNSIGNED NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao_video` longtext COLLATE utf8_unicode_ci,
  `id_subcategoriaproduto` int(11) DEFAULT NULL,
  `codigo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `src_youtube`, `descricao_video`, `id_subcategoriaproduto`, `codigo`) VALUES
(1, 'Produto 1 Lorem ipsum dolor sit amet', '1305201609401305887704..jpg', '<p>\r\n	Produto 1&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.&nbsp;</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Fogos de Artifícios 01', 'Fogos de Artifícios 01', 'Fogos de Artifícios 01', 'SIM', 0, 'produto-1-lorem-ipsum-dolor-sit-amet', 80, '3M', NULL, NULL, '', '', 1, '1001'),
(2, 'Produto 2 Lorem ipsum dolor sit amet', '1305201609401305887704..jpg', '<p>\r\n	Porta postigo direita Viena Ouro&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr.</p>\r\n<p>\r\n	Sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Fogos de Artifícios 02', 'Fogos de Artifícios 02', 'Fogos de Artifícios 02', 'SIM', 0, 'produto-2-lorem-ipsum-dolor-sit-amet', 77, 'Tigre', NULL, NULL, '', '', 3, '1002'),
(7, 'Produto 3 Lorem ipsum dolor sit amet', '1305201609401305887704..jpg', '<p>\r\n	Fogos de Artif&iacute;cios 0&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Fogos de Artifícios 03', 'Fogos de Artifícios 03', 'Fogos de Artifícios 03', 'SIM', 0, 'produto-3-lorem-ipsum-dolor-sit-amet', 82, 'Amanco', NULL, NULL, '', '', 4, '1234'),
(8, 'Produto 4 Lorem ipsum dolor sit amet', '1305201609401305887704..jpg', '<p>\r\n	4&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-4-lorem-ipsum-dolor-sit-amet', 80, 'Sol', NULL, NULL, '', '', 1, '1124'),
(9, 'Produto 5 Lorem ipsum dolor sit amet', '1305201609401305887704..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'SIM', 0, 'produto-5-lorem-ipsum-dolor-sit-amet', 81, 'Tigre', NULL, NULL, '', '', NULL, '1532'),
(10, 'Produto 6 Lorem ipsum dolor sit amet', '1305201609401305887704..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt</p>', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt', 'SIM', 0, 'produto-6-lorem-ipsum-dolor-sit-amet', 77, 'Solar', NULL, NULL, '', '', NULL, '7443'),
(11, 'Produto 7 Lorem ipsum dolor sit amet', '1305201609401305887704..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-7-lorem-ipsum-dolor-sit-amet', 77, 'Mais Luz', NULL, NULL, 'https://www.youtube.com/embed/xIM22tZAdwE', '<p>\r\n	Apresenta&ccedil;&atilde;o do v&iacute;deo Lorem Ipsum is simply dummy text of the printing and typesettin g industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a gall ey of type and scrambled it to make a type specimen book. It h as survived not only five centuries, but alsothe leap into electro nic typesetting, remaining essentially unchanged. It was popularis ed in the 1960</p>', NULL, '3568'),
(12, 'Produto 8 Lorem ipsum dolor sit amet', '1305201609401305887704..jpg', '<p>\r\n	Produto 8 Lorem ipsum dolor sit amet&nbsp;Produto 8 Lorem ipsum dolor sit amet&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-8-lorem-ipsum-dolor-sit-amet', 75, 'Tigre', NULL, NULL, '', '', NULL, '1346'),
(13, 'Produto 9 Lorem ipsum dolor sit amet', '1305201609401305887704..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-9-lorem-ipsum-dolor-sit-amet', 74, 'Amanco', NULL, NULL, '', '', NULL, '7542'),
(14, 'Produto 1001 Lorem ipsum dolor sit amet', '1305201609401305887704..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'produto-1001-lorem-ipsum-dolor-sit-amet', 82, 'Lual', NULL, NULL, '', '', 4, '2456');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_seo`
--

CREATE TABLE `tb_seo` (
  `idseo` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `title_google` longtext,
  `description_google` longtext,
  `keywords_google` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_seo`
--

INSERT INTO `tb_seo` (`idseo`, `titulo`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`) VALUES
(1, 'Index', 'Index', NULL, NULL, 'SIM', NULL, NULL),
(2, 'Empresa', 'Empresa', NULL, NULL, 'SIM', NULL, NULL),
(3, 'Serviços', 'Serviços', NULL, NULL, 'SIM', NULL, NULL),
(4, 'Produtos', 'Produtos', NULL, NULL, 'SIM', NULL, NULL),
(5, 'Dicas', 'Dicas', NULL, NULL, 'SIM', NULL, NULL),
(6, 'Contatos', 'Contatos', NULL, NULL, 'SIM', NULL, NULL),
(7, 'Trabalhe conosco', 'Trabalhe conosco', NULL, NULL, 'SIM', NULL, NULL),
(8, 'Orçamentos', 'Orçamentos', NULL, NULL, 'SIM', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) UNSIGNED NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_icone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`, `imagem_icone`) VALUES
(40, 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 1', '<p>Serviço 1 \r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '1105201612361272431186..jpg', 'SIM', NULL, 'titulo-do-servico-grande-para-teste-1', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 1', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 1', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 1', 0, '', '', '2104201610131175693451.png'),
(41, 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 2', '<p>Serviço 2\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '1105201612361272431186..jpg', 'SIM', NULL, 'titulo-do-servico-grande-para-teste-2', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 2', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 2', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 2', 0, '', '', '2104201610131348432865.png'),
(42, 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 3', '<p>Serviço 3\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '1105201612361272431186..jpg', 'SIM', NULL, 'titulo-do-servico-grande-para-teste-3', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 3', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 3', 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 3', 0, '', '', '2104201610131240709875.png'),
(43, 'TÍTULO DO SERVIÇO GRANDE PARA TESTE  DO SERVIÇO GRANDE PARA TESTE 4', '<p>Serviço 4\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '1105201612361272431186..jpg', 'SIM', NULL, 'titulo-do-servico-grande-para-teste-4', '', '', '', 0, '', '', '2104201610141153328479.png'),
(44, 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 5', '<p>Serviço 5\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '1105201612361272431186..jpg', 'SIM', NULL, 'titulo-do-servico-grande-para-teste-5', '', '', '', 0, '', '', '2104201610431210084250.png'),
(45, 'TÍTULO DO SERVIÇO GRANDE PARA TESTE 6', '<p>Serviço 6\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '1105201612361272431186..jpg', 'SIM', NULL, NULL, NULL, NULL, NULL, 0, '', '', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_banners`
--
ALTER TABLE `tb_banners`
  ADD PRIMARY KEY (`idbanner`);

--
-- Indexes for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  ADD PRIMARY KEY (`idbannerinterna`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_equipamentos`
--
ALTER TABLE `tb_equipamentos`
  ADD PRIMARY KEY (`idequipamento`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_seo`
--
ALTER TABLE `tb_seo`
  ADD PRIMARY KEY (`idseo`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_banners`
--
ALTER TABLE `tb_banners`
  MODIFY `idbanner` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_banners_internas`
--
ALTER TABLE `tb_banners_internas`
  MODIFY `idbannerinterna` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_equipamentos`
--
ALTER TABLE `tb_equipamentos`
  MODIFY `idequipamento` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_seo`
--
ALTER TABLE `tb_seo`
  MODIFY `idseo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
