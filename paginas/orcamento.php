<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 8);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 7) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 190px no-repeat;
  }
</style>





<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- TITULO BG INTERNAS  -->
  <!-- ======================================================================= -->
  <div class="container-fluid posicao_barra">
    <div class="row">
      <div class="barra_bg_internas"></div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12 text-center top220">
           <div class="col-xs-offset-4  titulo_pagina_internas effect2">
             <h1 class="pt25">ORÇAMENTOS</h1>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 <!-- ======================================================================= -->
 <!-- TITULO BG INTERNAS  -->
 <!-- ======================================================================= -->



<form class="form-inline FormContato" role="form" method="post">

<?php
    //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
        if(isset($_POST[nome])){

          //  CADASTRO OS PRODUTOS SOLICITADOS
          for($i=0; $i < count($_POST[qtd]); $i++){
            $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

            $itens .= "
                      <tr>
                        <td><p>". $dados[codigo] ."</p></td>
                        <td><p>". $_POST[qtd][$i] ."</p></td>
                        <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                      </tr>
                      ";
          }


          //  CADASTRO OS SERVICOS SOLICITADOS
          for($i=0; $i < count($_POST[qtd_servico]); $i++)
          {         
              $dados = $obj_site->select_unico("tb_servicos", "idservico", $_POST[idservico][$i]);
              
              $itens .= "
                          <tr>
                              <td><p></p></td>
                              <td><p>". $_POST[qtd_servico][$i] ."</p></td>
                              <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                           </tr>
                          ";
          }


          if (count($_POST[categorias]) > 0) {
            foreach($_POST[categorias] as $cat){
              $desc_cat .= $cat . ' , ';
            }
          }


                    //  ENVIANDO A MENSAGEM PARA O CLIENTE
         $texto_mensagem = "
          O seguinte cliente fez uma solicitação pelo site. <br />

          Nome: $_POST[nome] <br />
          Email: $_POST[email] <br />
          Telefone: $_POST[telefone] <br />
          FAX: $_POST[fax] <br />
          ENDEREÇO:$_POST[endereco] <br />
          Cidade: $_POST[cidade] <br />
          Estado: $_POST[estado] <br />
          Tipo de Pessoa:Estado: $_POST[tipoPessoa] <br />
          Como conheceu nosso site: $_POST[como_conheceu] <br />

          Mensagem: <br />
          ". nl2br($_POST[mensagem]) ." <br />

          <br />
          <h2> Produtos selecionados:</h2> <br />

          <table width='100%' border='0' cellpadding='5' cellspacing='5'>
            <tr>
              <td><h4>CÓDIGO</h4></td>
              <td><h4>QTD</h4></td>
              <td><h4>DESCRIÇÃO</h4></td>
            </tr>
            $itens
          </table>

          ";

        

          Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
          Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), $texto_mensagem, $nome_remetente, $email);
          unset($_SESSION[solicitacoes_produtos]);
          unset($_SESSION[solicitacoes_servicos]);
          unset($_SESSION[piscinas_vinil]);
          Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

}
?>






<!--  ==============================================================  -->
<!-- carrinho-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row">

    <div class="col-xs-12 tb-lista-itens top30">
      <h6 class="bottom20"><span>PRODUTOS SELECIONADOS</span></h6>
      <table class="table top10">

       <thead>
        <tr>
          <th class="text-center">ITEM</th>
          <th>DESCRIÇÃO</th>
          <th class="text-center">QUANTIDADE</th>
          <th class="text-center">DELETAR</th>
        </tr>
      </thead>

      <tbody>

        <?php
          if(count($_SESSION[solicitacoes_produtos]) > 0){
            for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
              $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
            ?>
              
                    <tr>
                      <td>
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 135, 85, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                      </td>
                      <td align="left"><?php Util::imprime($row[titulo]) ?></td>
                      <td class="text-center">
                          <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                          <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                      </td>
                      <td class="text-center">
                        <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Retirar do orçamento">
                          <img src="<?php echo Util::caminho_projeto() ?>/imgs/excluir.jpg" alt="">
                        </a>
                      </td>
                    </tr>
                  
            <?php 
            }
          }
          ?>


          <?php
          if(count($_SESSION[solicitacoes_servicos]) > 0){
            for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
              $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
            ?>
              
                    <tr>
                      <td>
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 135, 85, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                      </td>
                      <td align="left"><?php Util::imprime($row[titulo]) ?></td>
                      <td class="text-center">
                          <input type="text" class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                          <input name="idservico[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                      </td>
                      <td class="text-center">
                        <a href="?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Retirar do orçamento">
                          <img src="<?php echo Util::caminho_projeto() ?>/imgs/excluir.jpg" alt="">
                        </a>
                      </td>
                    </tr>
                  
            <?php 
            }
          }
          ?>

        

      </tbody>







    </table>

   
  </div>
</div>
</div>
<!--  ==============================================================  -->
<!-- carrinho -->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!-- FORMULARIO ENVIE ORCAMENTO-->
<!--  ==============================================================  -->
<div class="container">
  <div class="row formulario">
    <div class="top50 bottom30">
      <h6 class="bottom20 left20"><span>CONFIRME SEUS DADOS</span></h6>

        <div class="col-xs-12">

          <div class="col-xs-6 top15 form-group has-feedback">
            <input type="text" name="nome"  placeholder="NOME" class="form-control fundo-form1 input-lg input100">
          </div>

          <div class="col-xs-6 top15 form-group has-feedback">      
            <input type="text" name="email"  placeholder="E-MAIL" class="form-control fundo-form1 input-lg input100">
          </div>


          <div class="clearfix"></div>

          <div class="col-xs-6 top15 form-group has-feedback">      
            <input type="text" name="telefone"  placeholder="TELEFONE" class="form-control fundo-form1 input-lg input100">
          </div>

          <div class="col-xs-6 top15 form-group has-feedback">      
            <input type="text" name="fax"  placeholder="FAX" class="form-control fundo-form1 input-lg input100">
          </div>

          <div class="clearfix"></div>

          <div class="col-xs-6 top15 form-group has-feedback">      
            <input type="text" name="endereco"  placeholder="ENDEREÇO" class="form-control fundo-form1 input-lg input100">
          </div>

          
          <div class="col-xs-6 top15 form-group has-feedback">      
            <input type="text" name="estado"  placeholder="ESTADO" class="form-control fundo-form1 input-lg input100">
          </div>
          <div class="clearfix"></div>

          <div class="col-xs-6 top15 form-group has-feedback">      
            <input type="text" name="cidade"  placeholder="CIDADE" class="form-control fundo-form1 input-lg input100">
          </div>

          <div class="col-xs-6 top15 form-group has-feedback">      
            <input type="text" name="tipoPessoa"  placeholder="TIPO DE PESSOA" class="form-control fundo-form1 input-lg input100">
          </div>

          <div class="clearfix"></div>


          <div class="col-xs-12 form-group top15">
            <textarea name="mensagem"  cols="30" rows="9" class="form-control fundo-form1 input100 input-lg" placeholder="MENSAGEM"></textarea>
          </div>


          <div class="top45 col-xs-12">
            <button type="submit" class="btn btn-formulario pull-right">ENVIAR</button>
          </div>

          <!-- form fale conosco -->
        </div>

     


    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!-- FORMULARIO ENVIE ORCAMENTO-->
<!--  ==============================================================  -->







<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      endereco: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      
      tipoPessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      como_conheceu: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
