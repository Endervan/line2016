<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 4);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 5) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 190px no-repeat;
  }
</style>





<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- TITULO BG INTERNAS  -->
  <!-- ======================================================================= -->
  <div class="container-fluid posicao_barra">
    <div class="row">
      <div class="barra_bg_internas"></div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12 text-center top220">
           <div class="col-xs-offset-4  titulo_pagina_internas effect2">
             <h1 class="pt20">PRODUTOS</h1>
             <P class="right20">EM DESTAQUES</P>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 <!-- ======================================================================= -->
 <!-- TITULO BG INTERNAS  -->
 <!-- ======================================================================= -->


 <!--  ==============================================================  -->
  <!--PRODUTOS HOME-->
  <!--  ==============================================================  -->
  <div class="container produtos_cat top50">
    <div class="row">
     <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">

       <!--  ==============================================================  -->
       <!--PRODUTOS HOME CAT-->
       <!--  ==============================================================  -->
      <div class="col-xs-4 col-xs-offset-3">
        <select name="select_categorias" class="selectpicker" data-live-search="true" title="CATEGORIAS" data-width="100%">
          <optgroup label="SELECIONE">
            <?php 
            $result = $obj_site->select("tb_categorias_produtos");
            if (mysql_num_rows($result) > 0) {
              while($row = mysql_fetch_array($result)){
              ?>
                <option value="<?php Util::imprime($row[0]); ?>" data-tokens="<?php Util::imprime($row[titulo]); ?>"><?php Util::imprime($row[titulo]); ?></option>
              <?php 
              }
            }
            ?>
          </optgroup>
        </select>
      </div>

      <div class="col-xs-5 top15">
        <!-- barra pesquisas -->
          
            <div class="input-group stylish-input-group">
              <input type="text" class="form-control" name="busca_produtos" placeholder="PESQUISAR" >
              <span class="input-group-addon">
                <button type="submit">
                  <span class="glyphicon glyphicon-search"></span>
                </button>  
              </span>
            </div>
        
        <!-- barra pesquisas -->

      </div>
      <!--  ==============================================================  -->
      <!--PRODUTOS HOME CAT-->
      <!--  ==============================================================  -->
    </form>
  </div>
</div>
<!--  ==============================================================  -->
<!--PRODUTOS HOME-->
<!--  ==============================================================  -->








<div class="container">
  <div class="row top0">
    <!--  ==============================================================  -->
    <!-- MEnu lateral-->
    <!--  ==============================================================  -->
    <div class="col-xs-3 menu-produtos">
        <?php $url1 = Url::getURL(1); ?>
        <ul class="nav nav-pills nav-stacked text-center">
            
            <!--menu destaques-->
            <li class="top20 <?php if(!isset( $url1 )){ echo 'menu-destaques'; }else{ echo 'menu-produtos-cat'; } ?>">
              <a href="<?php echo Util::caminho_projeto() ?>/produtos">
                <span class="pull-left">
                  <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/produto_home01.png" alt="">
                </span>VER TODOS
              </a>
            </li>
            <!--menu destaques-->
            

            <?php 
            $result = $obj_site->select("tb_categorias_produtos");
            if (mysql_num_rows($result) > 0) {
              while($row = mysql_fetch_array($result)){
              ?>
                <li class="menu-produtos-cat  <?php if( $url1 == "$row[url_amigavel]" ){ echo 'menu-destaques'; } ?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    <span class="pull-left">
                      <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                    </span>
                      <?php Util::imprime($row[titulo]); ?>
                  </a>
                </li>
              <?php
              }
            }
            ?>

        </ul>



    





    <?php
    $result = $obj_site->select("tb_banners", "and tipo_banner = 3 ");
    if(mysql_num_rows($result) > 0){
      $i = 0;
      while ($row = mysql_fetch_array($result)) {
      ?>
          <div class="top10">
              <?php if (!empty($row[url])): ?>
                <a href="<?php Util::imprime($imagem[url]); ?>">
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 263, 266, array("class"=>"", "alt"=>"$row[titulo]")) ?>
                </a>
              <?php else: ?>
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 263, 266, array("class"=>"", "alt"=>"$row[titulo]")) ?>
              <?php endif; ?>
              
           </div>
      <?php
      }
    }
    ?>
    

   </div>
   <!--  ==============================================================  -->
   <!-- MEnu lateral-->
   <!--  ==============================================================  -->

   <!--  ==============================================================  -->
   <!-- PRODUTOS HOME -->
   <!--  ==============================================================  -->
   <div class="col-xs-9 produtos_home padding0">




     <!--  ==============================================================  -->
     <!-- ITEM 01 -->
     <!--  ==============================================================  -->
     <?php 
     $url1 = Url::getURL(1);

      //  FILTRA AS CATEGORIAS
      if (isset( $url1 )) {
          $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
          $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
      }

      //  FILTRA AS CATEGORIAS PELO SELECT
      if (isset( $_POST[select_categorias] ) and !empty($_POST[select_categorias])) {
          $complemento .= "AND id_categoriaproduto = '$_POST[select_categorias]' ";
      }

      //  FILTRA PELO TITULO
      if(isset($_POST[busca_produtos]) and !empty($_POST[busca_produtos]) ):
        $complemento = "AND titulo LIKE '%$_POST[busca_produtos]%'";
      endif;
    
      //  ORDENA PELO TITULO
      $complemento .= "order by titulo";   

      $result = $obj_site->select("tb_produtos", $complemento);
      if(mysql_num_rows($result) == 0){
        echo "<p class='bg-danger top20' style='padding: 20px;'>Nenhum produto encontrado.</p>";
      }else{
        $i = 0;
        while($row = mysql_fetch_array($result)){
        ?>
           <div class="col-xs-4">
            <div class="thumbnail">
              <a href="<?php echo Util::caminho_projeto(); ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 272, 169, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="caption">
                <h1><?php Util::imprime($row[titulo]); ?></h1>
                <div class="top20 bottom20">
                  <a href="<?php echo Util::caminho_projeto(); ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn_produtos_home_geral" role="button">Saiba Mais</a> 

                  <a class="btn btn-default btn_produtos_orcamento" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                    <button type="button" class="btn btn-default btn-circle btn-lg"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        <?php 
        if ($i == 2) {
          echo '<div class="clearfix"></div>';
          $i = 0;
        }else{
          $i++;
        }
      }
    }
    ?>






  </div>
  <!--  ==============================================================  -->
  <!-- PRODUTOS HOME -->
  <!--  ==============================================================  -->
</div>
</div>



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
