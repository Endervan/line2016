<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 5);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 2) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 190px no-repeat;
  }
</style>





<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- TITULO BG INTERNAS  -->
  <!-- ======================================================================= -->
  <div class="container-fluid posicao_barra">
    <div class="row">
      <div class="barra_bg_internas"></div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12 text-center top220">
           <div class="col-xs-offset-4  titulo_pagina_internas effect2">
             <h1 class="pt25">NOSSAS DICAS</h1>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 <!-- ======================================================================= -->
 <!-- TITULO BG INTERNAS  -->
 <!-- ======================================================================= -->


 <!--  ==============================================================  -->
 <!--DICAS DESCRICAO-->
 <!--  ==============================================================  -->
 <div class="container">
  <div class="row">
    <div class="col-xs-8 padding0">
      <!--  ==============================================================  -->
      <!--DICAS 01-->
      <!--  ==============================================================  -->

      <?php
      if(isset($_POST[busca_dicas])):
        $complemento = "and titulo LIKE '%$_POST[busca_dicas]%'";
      endif;

      $result = $obj_site->select("tb_dicas", $complemento);
      if(mysql_num_rows($result) == 0){
        echo '
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <h6 class="top80">
                Nenhum registro encontrado.
              </h6>
            </div>
          </div>
        </div>
        ';
      }else{ 
       $i = 0;
       while ($row = mysql_fetch_array($result)){
         ?>

         <div class="col-xs-12 dicas_desc top50 lista-dicas">
          <div class="thumbnail">
            <div class="top15 bottom10">
              <h1><?php Util::imprime($row[titulo]); ?></h1>
            </div>
            <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
             <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]",676, 322, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
           </a>
           <div class="caption top10 text-right">
            <p><?php Util::imprime($row[descricao], 380); ?></p>
            <div class="top10 bottom10">
              <a class="btn btn_dicas_saiba" href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>">
                Saiba Mais
              </a> 
            </div>
          </div>
        </div>
      </div>
      <?php 
      if ($i == 1) {
        echo '<div class="clearfix"></div>';
        $i = 0;
      }else{
        $i++;
      }

    }
  }
  ?>
</div>

<div class="col-xs-4 top35 categorias_dicas">

  <!-- barra pesquisas -->
  <form action="<?php echo Util::caminho_projeto() ?>/dicas/" method="post">
    <div class="input-group stylish-input-group">
      <input type="text" class="form-control" name="busca_dicas" placeholder="ENCONTRE DICA QUE PRECISA" >
      <span class="input-group-addon">
        <button type="submit">
          <span class="glyphicon glyphicon-search"></span>
        </button>  
      </span>
    </div>
  </form>
  <!-- barra pesquisas -->

  <!--  ==============================================================  -->
  <!--CATEGORIAS-->
  <!--  ==============================================================  -->
  <h6   class="top30">CATEGORIAS</h6>

  <ul>
    <?php 
    $result = $obj_site->select("tb_categorias_produtos", "order by rand() limit 8");
      if(mysql_num_rows($result) > 0){
        while ($row = mysql_fetch_array($result)) {
        ?>
          <li>
            <a href="<?php echo Util::caminho_projeto(); ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php Util::imprime($row[titulo]); ?>
            </a>
          </li>
        <?php
        }
      }
    ?>
  </ul>


  <!--  ==============================================================  -->
  <!--CATEGORIAS-->
  <!--  ==============================================================  -->
</div>

</div>
</div>
<!--  ==============================================================  -->
<!--DICAS DESCRICAO-->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
