<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 3) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 190px no-repeat;
  }
</style>





<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- TITULO BG INTERNAS  -->
  <!-- ======================================================================= -->
  <div class="container-fluid posicao_barra">
    <div class="row">
    <div class="barra_bg_internas"></div>

      <div class="container">
        <div class="row">
          <div class="col-xs-12 text-center top220">
           <div class="col-xs-offset-4  titulo_pagina_internas effect2">
             <h1 class="pt25">NOSSOS SERVIÇOS</h1>
           </div>
         </div>
       </div>
     </div>

   </div>
 </div>
 <!-- ======================================================================= -->
 <!-- TITULO BG INTERNAS  -->
 <!-- ======================================================================= -->


 
  
  <!-- ======================================================================= -->
  <!-- descricao servico  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row bg-cinza top35 pg10 bottom90">
      <div class="col-xs-12">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 7) ?>
            <p><?php Util::imprime($row[descricao]); ?></p>
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- descricao servico  -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- LISTA SERVICO  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row">
      
          <?php 
          $result = $obj_site->select("tb_servicos");
          if (mysql_num_rows($result) > 0) {
            while($row = mysql_fetch_array($result)){
            ?>
              <div class="col-xs-6">
                <div class="lista-servico">
                  <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                    
                    <div class="col-xs-2">
                        <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 68, 60, array("class"=>"", "alt"=>"$row[imagem]")) ?>
                    </div>

                    <div class="col-xs-10">
                        <h4><?php Util::imprime($row[titulo]); ?></h4>
                        <p><?php Util::imprime($row[descricao], 200); ?></p>
                    </div>

                  </a>
                </div>
              </div>
            <?php 
            }
          }
          ?>
      


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- LISTA SERVICO  -->
  <!-- ======================================================================= -->







<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
