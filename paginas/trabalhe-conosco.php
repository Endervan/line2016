<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 9) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 190px no-repeat;
  }
</style>





<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- TITULO BG INTERNAS  -->
  <!-- ======================================================================= -->
  <div class="container-fluid posicao_barra">
    <div class="row">
      <div class="barra_bg_internas"></div>

      <div class="container">
        <div class="row">
          <div class="col-xs-12 text-center top220">
           <div class="col-xs-offset-4  titulo_pagina_internas_trabalhe effect2">
             <h1 class="pt25">TRABALHE CONOSCO</h1>
           </div>
         </div>
       </div>
     </div>

   </div>
 </div>
 <!-- ======================================================================= -->
 <!-- TITULO BG INTERNAS  -->
 <!-- ======================================================================= -->




 <div class="container top50">
   <div class="row">
     <!--  ==============================================================  -->
     <!--CONTATOS -->
     <!--  ==============================================================  -->
     <div class="col-xs-4">
      <?php require_once("./includes/contato.php") ?>
    </div>
    <!--  ==============================================================  -->
    <!--CONTATOS -->
    <!--  ==============================================================  -->


    <!--  ==============================================================  -->
    <!-- FORMULARIO-->
    <!--  ==============================================================  -->
      <div class="col-xs-8">
          

          <?php
        //  VERIFICO SE E PARA ENVIAR O EMAIL
        if(isset($_POST[nome]))
        {
          
          if(!empty($_FILES[curriculo][name])):
            $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
            $texto = "Anexo: ";
            $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
            $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
          endif;

                $texto_mensagem = "
                                  Nome: ".$_POST[nome]." <br />
                                  Telefone: ".$_POST[telefone]." <br />
                                  Email: ".$_POST[email]." <br />
                                  Escolaridade: ".$_POST[escolaridade]." <br />
                                  Cargo: ".$_POST[cargo]." <br />
                                  Área: ".$_POST[area]." <br />
                                  Cidade: ".$_POST[cidade]." <br />
                                  Estado: ".$_POST[estado]." <br />
                                  Mensagem: <br />
                                  ".nl2br($_POST[mensagem])."

                                  <br><br>
                                  $texto    
                                  ";


                Util::envia_email($config[email], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
                Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] enviou um currículo"), $texto_mensagem, utf8_decode($_POST[nome]), ($_POST[email]));
                Util::alert_bootstrap("Obrigado por entrar em contato.");
                unset($_POST);
        }
        ?>



          <form class="form-inline FormContato" role="form" method="post" enctype="multipart/form-data"> 
            <div class="fundo-formulario">
              <!-- formulario orcamento -->
              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="nome" class="form-control fundo-form1 input100 input-lg" placeholder="NOME">
                    <span class="fa fa-user form-control-feedback top15"></span>        
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group  input100 has-feedback">
                    <input type="text" name="email" class="form-control fundo-form1 input100 input-lg" placeholder="E-MAIL">
                    <span class="fa fa-envelope form-control-feedback top15"></span>        
                  </div>
                </div>
              </div>

              <div class="clearfix"></div>   


             <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="telefone" class="form-control fundo-form1 input100  input-lg" placeholder="TELEFONE">
                    <span class="fa fa-phone form-control-feedback top15"></span>        
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="escolaridade" class="form-control fundo-form1 input100  input-lg" placeholder="ESCOLARIDADE">
                    <span class="glyphicon glyphicon-book form-control-feedback top10"></span>        
                  </div>
                </div>
              </div>

              <div class="clearfix"></div> 

              <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback">
                    <input type="text" name="cargo" class="form-control fundo-form1 input100  input-lg" placeholder="CARGO" >
                    <span class="glyphicon glyphicon-lock form-control-feedback top10"></span>        
                  </div>
                </div>

                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="area" class="form-control fundo-form1 input100  input-lg" placeholder="AREA">
                    <span class="glyphicon glyphicon-briefcase form-control-feedback top10"></span>        
                  </div>
                </div>
              </div>

               <div class="clearfix"></div> 

              <div class="top20">

                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="cidade" class="form-control fundo-form1 input100  input-lg" placeholder="CIDADE">
                    <span class="glyphicon glyphicon-globe form-control-feedback top10"></span>        
                  </div>
                </div>
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="text" name="estado" class="form-control fundo-form1 input100  input-lg" placeholder="ESTADO">
                    <span class="glyphicon glyphicon-globe form-control-feedback top10"></span>        
                  </div>
                </div>

              </div>

        
              <div class="clearfix"></div>

               <div class="top20">
                <div class="col-xs-6">
                  <div class="form-group input100 has-feedback ">
                    <input type="file" name="curriculo" class="form-control fundo-form1 input100  input-lg" placeholder="CURRÍCULO">
                    <span class="glyphicon glyphicon-file form-control-feedback top10"></span>        
                  </div>
                </div>
                </div>

              <div class="clearfix"></div>


              <div class="top20">
                <div class="col-xs-12">        
                 <div class="form-group input100 has-feedback">
                  <textarea name="mensagem" cols="30" rows="11" class="form-control fundo-form1 input100" placeholder="MENSAGEM"></textarea>
                  <span class="fa fa-pencil form-control-feedback top15"></span> 
                </div>
              </div>
            </div>

            <!-- formulario orcamento -->
            <div class="col-xs-12 text-right">
              <div class="top15 bottom25">
                <button type="submit" class="btn btn-vermelho" name="btn_contato">
                  ENVIAR
                </button>
              </div>
            </div>

          </div>

          <!--  ==============================================================  -->
          <!-- FORMULARIO-->
          <!--  ==============================================================  -->
        </form>
      </div>



    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- mapa -->
  <!--  ==============================================================  -->
  <div class="container-fluid top50">
    <div class="row">
      <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>

    </div>
  </div>
  <!--  ==============================================================  -->
  <!-- mapa -->
  <!--  ==============================================================  -->


</div>
</div>
<!--  ==============================================================  -->
<!-- CONTATOS E FORMULARIO-->
<!--  ==============================================================  -->




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
  });
</script>

