<?php 

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 1) ?>
<style>
    .bg-interna{
      background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 190px no-repeat;
    }
</style>



<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- CONHECA MAIS  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top55 desc-empresa">
      <div class="col-xs-7 text-right">
            <h1>CONHEÇA MAIS A</h1>
            <h1><span>LINE MATERIAIS ELÉTRICOS</span></h1>

            <div class="bg-branco pg20">
              <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3) ?>
              <div class="desc-mais-line">
                <p><?php Util::imprime($row[descricao]); ?></p>
              </div>
              
              <div class="text-left top10">
                <a href="" class="btn btn-vermelho-1">
                  MAIS SOBRE O GRUPO
                  <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                </a>
              </div>

            </div>

            
      </div>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CONHECA MAIS  -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- atendimento e grupo  -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top240 atendimento">
      <div class="col-xs-3">
            <?php require_once("./includes/contato.php") ?>
      </div>

      
      <!-- grupo line -->
      <div class="col-xs-3">
          <div class="border-empresa-grupo">
            <div class="text-center">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-grupo-line-eletrica.jpg" alt="" class="logo-grupo">
            </div>
            
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4) ?>
            <p class="top10 bottom10"><?php Util::imprime($row[descricao]); ?></p>
          </div>
      </div>
      <!-- grupo line -->


      <!-- grupo engenharia -->
      <div class="col-xs-3">
          <div class="border-empresa-grupo">
            <div class="text-center">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-grupo-line-engenharia.jpg" alt="" class="logo-grupo">
            </div>
            
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5) ?>
            <p class="top10 bottom10"><?php Util::imprime($row[descricao]); ?></p>
          </div>
      </div>
      <!-- grupo engenharia -->


      <!-- grupo ar condicionado -->
      <div class="col-xs-3">
          <div class="border-empresa-grupo">
            <div class="text-center">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/loo-line-ar-condicionado.jpg" alt="" class="logo-grupo">
            </div>
            
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6) ?>
            <p class="top10 bottom10"><?php Util::imprime($row[descricao]); ?></p>
          </div>
      </div>
      <!-- grupo ar condicionado -->


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- atendimento e grupo  -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!-- MAPS  -->
  <!-- ======================================================================= -->
  <div class="container-fluid ">
    <div class="row relative top50">
      <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="840" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- MAPS  -->
  <!-- ======================================================================= -->



  

<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
