<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>



<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

  <!-- slider JS files -->
  <script  src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery-1.8.0.min.js"></script>
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/royalslider.css" rel="stylesheet">
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery.royalslider.min.js"></script>
  <script>
    jQuery(document).ready(function($) {
            // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
            // it's recommended to disable them when using autoHeight module
            $('#content-slider-1').royalSlider({
              autoHeight: true,
              arrowsNav: false,
              fadeinLoadedSlide: false,
              controlNavigationSpacing: 0,
              controlNavigation: 'tabs',
              imageScaleMode: 'none',
              imageAlignCenter: false,
              loop: false,
              loopRewind: true,
              numImagesToPreload: 6,
              keyboardNavEnabled: true,
              usePreloader: false,
              autoPlay: {
                    // autoplay options go gere
                    enabled: true,
                    pauseOnHover: true
                  }

                });
          });
        </script>

        <script type="text/javascript">
          $(document).ready(function() {
            $('#Carousel').carousel({
              interval: 4000
            })
          });
        </script>




      </head>
      <body>


        <!-- ======================================================================= -->
        <!-- topo    -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/topo.php') ?>
        <!-- ======================================================================= -->
        <!-- topo    -->
        <!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- BG HOME    -->
<!-- ======================================================================= -->
<div class="container-fluid bottom40">
  <div class="row">

    <div id="container_banner">

      <div class="container_banner_faixa"></div>

      <div id="content_slider">
        <div id="content-slider-1" class="contentSlider rsDefault">

          <?php
          $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

          if (mysql_num_rows($result) > 0) {
            while ($row = mysql_fetch_array($result)) {
              ?>
              <!-- ITEM -->
              <div>
                <?php
                if ($row[url] == '') {
                  ?>
                  <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="1000" />
                  <?php
                }else{
                  ?>
                  <a href="<?php Util::imprime($row[url]) ?>">
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="1000" />
                  </a>
                  <?php
                }
                ?>

              </div>
              <!-- FIM DO ITEM -->
              <?php
            }
          }
          ?>

        </div>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!-- BG HOME    -->
<!-- ======================================================================= -->



<!--  ==============================================================  -->
<!--PRODUTOS HOME-->
<!--  ==============================================================  -->
<div class="container produtos_cat">
  <div class="row top40">
    <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
     <!--  ==============================================================  -->
     <!--PRODUTOS HOME CAT-->
     <!--  ==============================================================  -->
     <div class="col-xs-3 lato-black text-right">
      <h1>NOSSOS</h1>
      <h2>PRODUTOS</h2>


    </div>

    <div class="col-xs-4">
      <select name="select_categorias" class="selectpicker" data-live-search="true" title="CATEGORIAS" data-width="100%">
        <optgroup label="SELECIONE">
          <?php
          $result = $obj_site->select("tb_categorias_produtos");
          if (mysql_num_rows($result) > 0) {
            while($row = mysql_fetch_array($result)){
              ?>
              <option value="<?php Util::imprime($row[0]); ?>" data-tokens="<?php Util::imprime($row[titulo]); ?>"><?php Util::imprime($row[titulo]); ?></option>
              <?php
            }
          }
          ?>
        </optgroup>
      </select>
    </div>

    <div class="col-xs-5 top15">
      <!-- barra pesquisas -->
      <form action="<?php echo Util::caminho_projeto() ?>/equipamentos/" method="post">
        <div class="input-group stylish-input-group">
          <input type="text" class="form-control" name="busca_equipamentos" placeholder="PESQUISAR" >
          <span class="input-group-addon">
            <button type="submit">
              <span class="glyphicon glyphicon-search"></span>
            </button>
          </span>
        </div>
      </form>
      <!-- barra pesquisas -->

    </div>
    <!--  ==============================================================  -->
    <!--PRODUTOS HOME CAT-->
    <!--  ==============================================================  -->
  </form>
</div>
</div>
<!--  ==============================================================  -->
<!--PRODUTOS HOME-->
<!--  ==============================================================  -->








<div class="container">
  <div class="row">
    <!--  ==============================================================  -->
    <!-- MEnu lateral-->
    <!--  ==============================================================  -->
    <div class="col-xs-3 menu-produtos">
      <ul class="nav nav-pills nav-stacked text-center">
        <!--menu destaques-->
        <li class="menu-destaques top20">
          <a href="<?php echo Util::caminho_projeto() ?>/produtos">
            <span class="pull-left">
              <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/imgs/produto_home01.png" alt="">
            </span>VER TODOS
          </a>
        </li>
        <!--menu destaques-->


        <?php
        $result = $obj_site->select("tb_categorias_produtos", "order by ordem limit 6");
        if (mysql_num_rows($result) > 0) {
          while($row = mysql_fetch_array($result)){
            ?>
            <li class="menu-produtos-cat">
              <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <span class="pull-left">
                  <img class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>">
                </span>
                <?php Util::imprime($row[titulo]); ?>
              </a>
            </li>
            <?php
          }
        }
        ?>

      </ul>


      <?php
      $result = $obj_site->select("tb_banners", "and tipo_banner = 3 order by rand() limit 1 ");
      if(mysql_num_rows($result) > 0){
        $i = 0;
        while ($row = mysql_fetch_array($result)) {
          ?>
          <div class="top10">
            <?php if (!empty($row[url])): ?>
            <a href="<?php Util::imprime($imagem[url]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 263, 266, array("class"=>"", "alt"=>"$row[titulo]")) ?>
            </a>
          <?php else: ?>
          <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 263, 266, array("class"=>"", "alt"=>"$row[titulo]")) ?>
        <?php endif; ?>

      </div>
      <?php
    }
  }
  ?>

</div>
<!--  ==============================================================  -->
<!-- MEnu lateral-->
<!--  ==============================================================  -->

<!--  ==============================================================  -->
<!-- PRODUTOS HOME -->
<!--  ==============================================================  -->
<div class="col-xs-9 produtos_home padding0">



  <?php
  $result = $obj_site->select("tb_produtos", "order by ordem limit 6");
  if (mysql_num_rows($result) > 0) {
    $i = 0;
    while($row = mysql_fetch_array($result)){
      ?>
      <div class="col-xs-4">
        <div class="thumbnail">
          <a href="<?php echo Util::caminho_projeto(); ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 272, 169, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
          </a>
          <div class="caption">
            <h1><?php Util::imprime($row[titulo]); ?></h1>
            <div class="top20 bottom20">
              <a href="<?php echo Util::caminho_projeto(); ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn_produtos_home_geral" role="button">
                Saiba Mais</a>
                <a class="btn btn-default btn_produtos_orcamento left10" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                  <button type="button" class="btn btn-default btn-circle btn-lg"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                </a>
              </div>
            </div>
          </div>
        </div>
        <?php
        if ($i == 2) {
          echo '<div class="clearfix"></div>';
          $i = 0;
        }else{
          $i++;
        }
      }
    }
    ?>

    <a href="<?php echo Util::caminho_projeto(); ?>/produtos" title="VER TODOS" class="btn btn_produtos_todos pull-right">
      VER TODOS
    </a>
  </div>
  <!--  ==============================================================  -->
  <!-- PRODUTOS HOME -->
  <!--  ==============================================================  -->
</div>
</div>


<!--  ==============================================================  -->
<!--  OUTROS SERVICOS HOME-->
<!--  ==============================================================  -->
<div class="container top100 bottom40">
  <div class="row">
    <div class="col-xs-4 servicos_home">
      <h1>OS NOSSOS SERVIÇOS</h1>
      <div class="top30 bottom10">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 8) ?>
        <p><?php Util::imprime($row[descricao], 10000); ?></p>
      </div>
      <div class="text-center">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/seta_outros_servicos.png" alt="">
      </div>
      <div class="top30">
        <a href="<?php echo Util::caminho_projeto() ?>/orcamento" class="btn btn_orcamentos" role="button">SOLICITE UM ORÇAMENTO</a>
      </div>
    </div>

    <div class="col-xs-8 padding0 servicos_home_cat">

     <div class="text-right">
      <a href="<?php echo Util::caminho_projeto(); ?>/servicos/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn_servicos_home_geral" role="button">
        VER TODOS<i class="fa fa-plus-circle pull-right top5" aria-hidden="true"></i>
      </a>
    </div>

    <div class="clearfix"></div>
    <?php
    $result = $obj_site->select("tb_servicos", "order by rand() limit 6");
    if (mysql_num_rows($result) > 0) {
      $i = 0;
      while($row = mysql_fetch_array($result)){
        ?>
        <div class="col-xs-6 top30">
          <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
            <div class="media">
              <div class="media-left media-middle">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 68, 60, array("class"=>"media-object", "alt"=>"$row[imagem]")) ?>
              </div>
              <div class="media-body">
                <h1 class="media-heading"><?php Util::imprime($row[titulo]); ?></h1>
                <div class="top10">
                  <p><?php Util::imprime($row[descricao], 200); ?></p>
                </div>
              </div>
            </div>
          </a>
        </div>
        <?php
        if ($i == 1) {
          echo '<div class="clearfix"></div>';
          $i = 0;
        }else{
          $i++;
        }
      }
    }
    ?>




  </div>
</div>
</div>
<!--  ==============================================================  -->
<!--  OUTROS SERVICOS HOME-->
<!--  ==============================================================  -->


<!--  ==============================================================  -->
<!--SAIBA MAIS MATERIAIS-->
<!--  ==============================================================  -->
<div class="container-fluid saiba_mais_materiais">
  <div class="row">
    <div class="top100 text-center">
      <h5>SOBRE A LINE MATERIAIS ELÉTRICOS</h5>
    </div>

    <div class="container top90">
      <div class="row saiba_mais_desc">
        <div class="col-xs-7 top35">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 9) ?>
          <p><?php Util::imprime($row[descricao], 10000); ?></p>
        </div>

        <div class="col-xs-5">
          <div class="top35">
            <h6 class="bottom20">COMO CHEGAR</h6>
            <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="290" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>

          <div class="pull-left">
            <h3><i class="fa fa-phone-square right10"></i>ATENDIMENTO:</h3>
          </div>

          <div class="pull-left left10">
            <h4>
              <span>
                <?php Util::imprime($config[telefone1]); ?>
              </span>
            </h4>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
<!--  ==============================================================  -->
<!--SAIBA MAIS MATERIAIS-->
<!--  ==============================================================  -->

<div class="clearfix"></div>
<!--  ==============================================================  -->
<!--NOSSA DICAS-->
<!--  ==============================================================  -->
<div class="container top50">
  <div class="row dicas_home">
    <div class="col-xs-8">



      <?php
      $result = $obj_site->select("tb_dicas", "order by rand() limit 4");
      if (mysql_num_rows($result) > 0) {
        $i = 0;
        while($row = mysql_fetch_array($result)){
          ?>
          <div class="col-xs-6 top30">
            <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <div class="media">
                <div class="media-left media-middle">
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 102, 102, array("class"=>"media-object img-circle", "alt"=>"$row[imagem]")) ?>
                </div>
                <div class="media-body">
                  <div class="top10 media-heading">
                    <p><?php Util::imprime($row[titulo]); ?></p>
                  </div>
                </div>
              </div>
            </a>
          </div>
          <?php
          if ($i == 1) {
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }
        }
      }
      ?>



    </div>

    <div class="col-xs-4">
      <h6>AS NOSSAS DICAS</h6>
      <div class="top30 bottom15">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 10) ?>
        <p><?php Util::imprime($row[descricao], 10000); ?></p>
      </div>
      <div class="text-center">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/seta_dica_home.png" alt="">
      </div>
    </div>

  </div>
</div>
<!--  ==============================================================  -->
<!--NOSSA DICAS-->
<!--  ==============================================================  -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
