<?php 

// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>



</head>

<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  --> 
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 6) ?>
<style>
  .bg-interna{
    background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) center 190px no-repeat;
  }
</style>





<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!-- TITULO BG INTERNAS  -->
  <!-- ======================================================================= -->
  <div class="container-fluid posicao_barra">
    <div class="row">
      <div class="barra_bg_internas"></div>
      <div class="container">
        <div class="row">
          <div class="col-xs-12 text-center top220">
           <div class="col-xs-offset-4  titulo_pagina_internas effect2">
             <h1 class="pt20">PRODUTOS</h1>
             <P class="right20">EM DESTAQUES</P>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 <!-- ======================================================================= -->
 <!-- TITULO BG INTERNAS  -->
 <!-- ======================================================================= -->


 <!--  ==============================================================  -->
 <!--DESC PRODUTOS DENTRO-->
 <!--  ==============================================================  -->
 <div class="container produtos_dentro top40">
   <div class="row">
     <div class="col-xs-4 text-center">
      <?php $obj_site->redimensiona_imagem("../uploads/$dados_dentro[imagem]", 350, 325, array("class"=>"input100", "alt"=>"$dados_dentro[titulo]")) ?>

      <a class="btn btn_orcamento_produtos top20 input100" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
          ADICIONAR AO ORÇAMENTO<i class="fa fa-cart-arrow-down fa-2x pull-right top10" aria-hidden="true"></i>
        </a>

    </div>
    
    <div class="col-xs-8">
      <!--  ==============================================================  -->
      <!--AVALIACOES-->
      <!--  ==============================================================  -->
      <h1><?php Util::imprime($dados_dentro[titulo]); ?></h1>
      <h2>MARCA:<span class="left10"><?php Util::imprime($dados_dentro[marca]); ?></span></h2>
      <h2>CÓDIGO:<span class="left10"><?php Util::imprime($dados_dentro[codigo]); ?></span></h2>
        

      <!--  ==============================================================  -->
      <!--AVALIACOES-->
      <!--  ==============================================================  -->

      <div class="clearfix"></div>
      
      <div class="top30">
        <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
      </div>

      <div class="top20 text-right">
        <a class="btn btn_orcamento_produtos" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
          ADICIONAR AO ORÇAMENTO<i class="fa fa-cart-arrow-down fa-2x pull-right top10" aria-hidden="true"></i>
        </a>
      </div>
      
    </div>
    
  </div>
</div>
<!--  ==============================================================  -->
<!--DESC PRODUTOS DENTRO-->
<!--  ==============================================================  -->



<div class="container">
  <div class="row top30">
   
    <div class="col-xs-12">
      <h6>VEJA TAMBÉM</h6>
   </div>
  

   <!--  ==============================================================  -->
   <!-- PRODUTOS HOME -->
   <!--  ==============================================================  -->
   <div class="col-xs-12 top40 produtos_home padding0">

      <?php
      $result = $obj_site->select("tb_produtos", "order by rand() limit 4");
      if(mysql_num_rows($result) > 0){
        while($row = mysql_fetch_array($result)){
        ?>
           <div class="col-xs-3">
            <div class="thumbnail">
              <a href="<?php echo Util::caminho_projeto(); ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 272, 169, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="caption">
                <h1><?php Util::imprime($row[titulo]); ?></h1>
                <div class="top20 bottom20">
                  <a href="<?php echo Util::caminho_projeto(); ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" class="btn btn_produtos_home_geral" role="button">Saiba Mais</a> 

                  <a class="btn btn-default btn_produtos_orcamento" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                    <button type="button" class="btn btn-default btn-circle btn-lg"><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        <?php 
      }
    }
    ?>




  </div>
  <!--  ==============================================================  -->
  <!-- PRODUTOS HOME -->
  <!--  ==============================================================  -->
</div>
</div>


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>
